<?php

// запрет прямого обращения
define('ISHOP', TRUE);
session_start();

if($_GET['do']=="logout"){
    unset($_SESSION['auth']);
}

if(!$_SESSION['auth']['admin']){
   // подключение авторизации
   include $_SERVER['DOCUMENT_ROOT'].'/admin/auth/index.php';
}
// подключение файла конфигурации
require_once '../config.php';
// подключение файла функций
require_once '../functions/functions.php';
// подключение файла функций
require_once 'function/functions.php';
//управляющая страница
include 'controller/controller.php';

// HEADER
include ADMIN_TAMPLATE.'header.php';
// CONTENT
include ADMIN_TAMPLATE.$view.'.php';
// RIGHTBAR
include ADMIN_TAMPLATE.'right_bar.php';
// FOOTER
include ADMIN_TAMPLATE.'footer.php'; 
         
?>
