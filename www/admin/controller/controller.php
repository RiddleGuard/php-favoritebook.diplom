<?php 
defined('ISHOP') or die('Access denied'); 
// получение динамичной части шаблона #content
$view = empty($_GET['view']) ? 'pages' : $_GET['view'];

// удаление картинки
if($_POST['img']){
    $res = del_img();
    exit($res);
}

//получение количества необработанных заказов
$Count_new_orders = count_orders();

// получение массива каталога
$cat = catalog();
switch($view){
    case('pages'):
        // страницы
        $pages = pages();
    break;
    
    case('add_page'):
        //добавление страницы
        if($_POST){
            if(add_page($page_id)) redirect('?view=pages');
            else redirect(); 
        }
    break;
    case('del_page'):
        //удаление страницы
        $page_id = (int)$_GET['page_id'];
        del_page($page_id);
        redirect(); 
    break;
    case('edit_page'):
        //редактирование страницы
        $page_id = (int)$_GET['page_id'];
        $get_page= get_page($page_id);
        
        if($_POST){
            if(edit_page($page_id)) redirect('?view=pages');
            else redirect(); 
        }
    break;
    case('news'):
        //все новости
        $perpage = 10; // кол-во новостей на страницу
        if(isset($_GET['page'])){
            $page = (int)$_GET['page'];
            if($page < 1) $page = 1;
        }else{
            $page = 1;
        }
        $count_rows = count_news(); // общее кол-во новостей
        $pages_count = ceil($count_rows / $perpage); // общее кол-во страниц
        if(!$pages_count) $pages_count=1; //минимум одна страница
        if($page > $pages_count) $page = $pages_count;
        $start_pos = ($page -1)*$perpage; //начальная позиция для запроса
        
        $all_news = get_all_news($start_pos,$perpage);
    break;
    case('add_news'):
        if($_POST){
            if(add_news($news_id)) redirect('?view=news');
            else redirect(); 
        }
    break;
    case('edit_news'):
        //редактирование страницы
        $news_id = (int)$_GET['news_id'];
        $get_news= get_news($news_id);
        
        if($_POST){
            if(edit_news($news_id)) redirect('?view=news');
            else redirect(); 
        }
    break;
    case('del_news'):
        //удаление страницы
        $news_id = (int)$_GET['news_id'];
        del_news($news_id);
        redirect(); 
    break;
    case('categories'):
    
    break;
    case('add_categories'):
        if($_POST){
            if(add_categories()) redirect('?view=categories');
            else redirect(); 
        }
    break;
    case('edit_categories'):
        $categories_id = (int)$_GET['categories_id'];
        $parent_id = (int)$_GET['parent_id'];
        
        if($parent_id == $categories_id or !$parent_id){
            //если родительская категория
            $cat_name= $cat[$categories_id][0];
        }else{
            //если дочерняя категория
            $cat_name= $cat[$parent_id]['sub'][$categories_id];
        }
        if($_POST){
            if($parent_id and edit_categories($categories_id)){
                redirect("?view=cat&categories=$categories_id");
            }elseif(edit_categories($categories_id)){
                redirect("?view=categories");
            }else{
                redirect();
            }
        }
    break;
    case('add_product'):
    $categories_id =(int)$_GET['categories_id'];
        if($_POST){
            if(add_product($categories_id)){
                redirect("?view=cat&categories=$categories_id");
            }else{
                redirect();
            }
        }
    break;
    case('edit_product'):
        $goods_id = (int)$_GET['goods_id'];
        $get_product = get_product($goods_id);
        $categories_id = $get_product['categories_id'];
        // если есть базовая картинка
        if($get_product['image'] != "no_image.jpg"){
            $baseimg = '<img class="delimg" rel="0" width="48" src="' .PRODUCTIMG.$get_product['image']. '" alt="' .$get_product['image']. '">';
        }else{
            $baseimg = '<input type="file" name="baseimg" />';
        }
        if($_POST){
            if(edit_product($goods_id)) redirect("?view=cat&categories=$categories_id");
                else redirect();
        }
    break;
    case('del_product'):
        $goods_id = (int)$_GET['goods_id'];
        del_product($goods_id);
        redirect();
    break;
    case('del_categories'):
        $categories_id = (int)$_GET['categories_id'];
        del_categories($categories_id);
        redirect();
    break;
    case('del_orders'):
        $order_id = (int)$_GET['dels'];
        del_order($order_id);
        redirect();
    break;
    case('cat'):
        $category = (int)$_GET['categories'];
        $parent_id = (int)$_GET['parent_id'];
        // параметры для навигации
        $perpage = PERPAGE;// кол-товаров на страницу
        if(isset($_GET['page'])){
            $page = (int)$_GET['page'];
            if($page < 1) $page =1;
        }else{
            $page =1;
        }
        $count_rows = count_rows($category); //кол-во товаров на всех страницах
        $pages_count = ceil($count_rows / $perpage);  // общее кол-во страниц
        if(!$pages_count) $pages_count=1; //минимум одна страница
        if($page > $pages_count) $page = $pages_count;
        $start_pos = ($page -1)*$perpage; //начальная позиция для запроса
        
        $categories_name=categories_name($category);
        $products = products($category, $start_pos, $perpage); // получаем массив из модели
    break;
    case('orders'):
    if(isset($_GET['confirm'])){
        $order_id =(int)$_GET['confirm'];
        if(confirm_order($order_id)){
            $_SESSION['answer'] = "<div class = 'success'>Статус заказа №{$order_id} успешно изменен</div>";
            redirect("?view=orders&status=0");
        }else{
            $_SESSION['answer'] = "<div class = 'error'>Статус заказа №{$order_id} не удалось изменить.</div>";
            redirect("?view=orders&status=0");
        }
    }
        if($_GET['status'] === '0'){
            $status = "WHERE orders.status = '0'";
        }
        // параметры для навигации
        $perpage = PERPAGE;// кол-товаров на страницу
        if(isset($_GET['page'])){
            $page = (int)$_GET['page'];
            if($page < 1) $page =1;
        }else{
            $page =1;
        }
        $count_rows = count_orders(); //кол-во товаров на всех страницах
        $pages_count = ceil($count_rows / $perpage);  // общее кол-во страниц
        if(!$pages_count) $pages_count=1; //минимум одна страница
        if($page > $pages_count) $page = $pages_count;
        $start_pos = ($page -1)*$perpage; //начальная позиция для запроса
        $new_orders = orders($start_pos, $perpage,$status);
    break;
    case('show_order'):
        $order_id = (int)$_GET['order_id'];
        $show_order = show_order($order_id);
        if($show_order[0]['status']){
            $state = "обработан";
        }else{
            $state = "Не обработано";
        }
    break;
    
    default:
    // если из адресной строки получено имя несуществующего вида
    $view = 'pages';
    $pages = pages();
}

?>