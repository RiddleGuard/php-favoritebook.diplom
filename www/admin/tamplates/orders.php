<?php defined('ISHOP') or die('Access denied'); ?>
<div id="contentwrapper">
<div id="content">
<div class="content-main">
<div class="content">
	<h2>Новые заказы</h2>
<?php if($new_orders): ?>
<?php
if(isset($_SESSION['answer'])){
    echo $_SESSION['answer'];
    unset($_SESSION['answer']);
}
?>
	<table class="tabl" cellspacing="1">
	  <tr>
		<th class="number">№ заказа</th>
		<th class="str_name">Клиент</th>
		<th class="str_sort">Дата</th>
		<th class="str_action">Действие</th>
	  </tr>
      <?php foreach($new_orders as $item): ?>
<tr <?php if($item['status'] == 0 and $status == '') echo "class='tabelback'" ?>>
    <td><?=$item['order_id']?></td>
    <td class="name_page"><?=$item['name']?></td>
    <td><?=$item['date']?></td>
    <td><a href="?view=show_order&amp;order_id=<?=$item['order_id']?>" class="edit">Просмотр</a>&nbsp; | &nbsp;<a href="?view=del_orders&amp;dels=<?=$item['order_id']?>" class="del">удалить</a></td>
</tr>
    <?php endforeach; ?>
	</table>
<?php if($pages_count > 1) pagination($page, $pages_count); ?>
<?php else: ?>
    <div class="error">Нет необработанных заказов.</div>
<?php endif ?>
	</div> <!-- .content -->
	</div> <!-- .content-main -->
</div> <!-- .karkas -->
</div>