<?php defined('ISHOP') or die('Access denied'); ?>
<div id="contentwrapper">
<div id="content">
<div class="content-main">
<div class="content">
	<h2>Заказ - №<?=$order_id?> (<?=$state?>)</h2>
     <?php if(isset($_SESSION['answer'])){
                    echo $_SESSION['answer'];
                    unset ($_SESSION['answer']);
            }
            ?>
    <?php if($show_order): ?>
    <?php if($show_order[0]['status']==0):?>
    <a href="?view=orders&amp;confirm=<?=$order_id?>" class="edit">Подтвердить заказ</a>&nbsp; | 
    <?php endif;?>
    &nbsp;<a href="?view=del_orders&amp;dels=<?=$order_id?>" class="del">Удалить заказ</a>
    <br />
    <br />
    <table class="tabl" cellspacing="1">
	  <tr>
		<th class="number">№</th>
		<th class="str_name">Наименование</th>
		<th class="str_sort">Цена</th>
		<th class="str_action">Количество</th>
	  </tr>
      <?php $i =1; $totalsum  = 0?>
      <?php foreach($show_order as $item): ?>
        <tr>
            <td><?=$i?></td>
            <td class="name_page"><?=$item['name']?></td>
            <td><?=$item['price']?></td>
            <td><?=$item['quantity']?></td>
        </tr>
        <?php $i++; $totalsum += $item['quantity']*$item['price']?>
        <?php endforeach; ?>
        </table>
        <p>Общая цена заказа: <?=$totalsum?></p>
        <p>Дата заказа: <?=$item['date']?></p>
        <p>Способ доставки заказа: <?=$item['dostavkaname']?></p>
        <br />
        <p>Данные покупателя:</p>
        
        <table class="tabl" cellspacing="1">
	  <tr>
		<th class="str_name">Наименование</th>
		<th class="str_action">Адресс</th>
		<th class="str_action">Связь</th>
        <th class="str_name">Примечание</th>
	  </tr>
        <tr>
            <td><?=$item['username']?></td>
            <td class="name_page"><?=$item['address']?></td>
            <td><?=$item['email']?><br /><?=$item['phone']?></td>
            <td><?=$item['prim']?></td>
        </tr>
        </table>
    <?php else: ?>
        <div class="error">Такого заказа нет.</div>
    <?php endif; ?>
	</div> <!-- .content -->
	</div> <!-- .content-main -->
</div> <!-- .karkas -->
</div>