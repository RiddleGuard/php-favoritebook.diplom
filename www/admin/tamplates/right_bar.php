<?php defined('ISHOP') or die('Access denied'); ?>
    <div id="right-bar">
        <div class="right-bar-cont">
            <h2>Меню управления</h2>
            <ul class="nav-catalog-admin nav-catalog">
            <li><a href="?view=pages">Страницы</a></li>
            <h3><li><a href="?view=categories">Категории</a></li></h3>
                <ul class="nav-catalog" id="accordion">
                    <?php foreach($cat as $key => $item): ?>
                    <?php if(count($item) > 1): // если это родительская категория ?>
                    <h3><li><a href="#"><?=$item[0]?></a></li></h3>
                        <ul>
                            <?php foreach($item['sub'] as $key => $sub): ?>
                            <li>- <a href="?view=cat&amp;categories=<?=$key?>"><?=$sub?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php elseif($item[0]): // если самостоятельная категория ?>
                        <li><a href="?view=cat&categories=<?=$key?>"><?=$item[0]?></a></li>
                    <?php endif; ?>
                <?php endforeach; ?> 
                </ul>
            
            <li><a href="?view=news">новости</a></li>
            <li><a href="?view=orders">Заказы</a></li>
            <?php if($Count_new_orders > 0):?>
            <li><a href="?view=orders&amp;status=0" style="color: red;">Есть новые заказы(<?=$Count_new_orders?>)</a></li>
            <?php endif;?>
            </ul>
        </div>
  </div>