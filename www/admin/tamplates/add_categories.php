<?php defined('ISHOP') or die('Access denied'); ?>
<div id="contentwrapper">
<div id="content">
	<div class="content-main">
<div class="content">
	
<h2>Добавление категории</h2>
<?php
if(isset($_SESSION['add_categories']['res'])){
    echo $_SESSION['add_categories']['res'];
    unset($_SESSION['add_categories']);
}
?>

<form action="" method="post">
				
	<table class="add_edit_page" cellspacing="0" cellpadding="0">
	  <tr>
		<td class="add-edit-txt">Название категории:</td>
		<td><input class="head-text" type="text" name="categories_name" /></td>
	  </tr>
      <tr>
		<td>Родительская категория:</td>
		<td><select class="select-inf" name="parent_id">
        	<option value="0">Самостоятельная категория</option>
            <?php foreach($cat as $key => $value): ?>
            <option value="<?=$key?>"><?=$value[0]?></option>
            <?php endforeach; ?>
        </select></td>
        </tr>
	</table>
	
    <input type="image" src="<?=ADMIN_TAMPLATE?>images/save.png"  /> 
     

</form>

	</div> <!-- .content -->
	</div> <!-- .content-main -->
</div> <!-- .karkas -->
</div>  