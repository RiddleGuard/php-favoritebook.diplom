<?php defined('ISHOP') or die('Access denied'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="<?=ADMIN_TAMPLATE?>css/style.css">
<title>Административная панель</title>
<script type="text/javascript" src="<?=ADMIN_TAMPLATE?>js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_TAMPLATE?>js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_TAMPLATE?>js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?=ADMIN_TAMPLATE?>js/workscripts.js"></script>
<script type="text/javascript" src="<?=ADMIN_TAMPLATE?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?=ADMIN_TEMPLATE;?>js/ajaxupload.js"></script>
</head>

<body>
<div class="main">

	<div class="header">
        <a href="<?=PATH?>/Admin"><img class="logo" src="<?=ADMIN_TAMPLATE?>images/logo.png" alt="Favorite book" /></a>
        <p class="title_admin">Административная панель</p>
    </div>
    <div class="header2">
    
    <div class="enter"><b><a href="<?=PATH?>">На сайт</a> | <a href="?view=edit_user&amp;user_id=<?=$_SESSION['auth']['user_id']?>"><?=$_SESSION['auth']['admin']?></a> | <a href="?do=logout">Выход</a></b></div>
    </div>