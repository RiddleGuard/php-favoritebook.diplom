<?php defined('ISHOP') or die('Access denied'); ?>
<div id="contentwrapper">
<div id="content">
	<div class="content-main">
		<div class="content">
			<h2>Список категорий</h2>
            <?php if(isset($_SESSION['answer'])){
                    echo $_SESSION['answer'];
                    unset ($_SESSION['answer']);
            } ?>
			<table class="tabl" cellspacing="1">
			  <tr>
				<th class="number">№</th>
				<th class="str_name">Название страницы</th>
				<th class="str_action">Действие</th>
			  </tr>
        <?php $i=1; ?>
        <?php foreach($cat as $key => $item): ?>
			  <tr>
				<td><?=$i?></td>
				<td class="name_page"><?=$item[0]?></td>
				
				<td><a href="?view=edit_categories&amp;categories_id=<?=$key?>" class="edit">изменить</a>&nbsp; | &nbsp;<a href="?view=del_categories&amp;categories_id=<?=$key?>" class="del">удалить</a></td>
			  </tr>
        <?php $i++; ?>
		<?php endforeach; ?>	  
			</table>
			<div class="btn_content"><a href="?view=add_categories">Добавить категорию</a></div>

		</div> <!-- .content -->
	</div> <!-- .content-main -->
</div>
</div>