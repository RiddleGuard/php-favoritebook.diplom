<?php defined('ISHOP') or die('Access denied'); ?>
<div id="contentwrapper">
<div id="content">
	<div class="content-main">
		<div class="content">
	<h2>Добавление товара</h2>
<?php
if(isset($_SESSION['add_product']['res'])){
    echo $_SESSION['add_product']['res'];
}
?>

<form action="" method="post" enctype="multipart/form-data">
				
	<table class="add_edit_page" cellspacing="0" cellpadding="0">
	  <tr>
		<td class="add-edit-txt">Название товара:</td>
		<td><input class="head-text" type="text" name="name" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Цена:</td>
		<td><input class="head-text" type="text" name="price" value="<?=$_SESSION['add_product']['price']?>" /></td>
	  </tr>
       <tr>
		<td class="add-edit-txt">Ключевые слова</td>
		<td><input class="head-text" type="text" name="keywords" value="<?=$_SESSION['add_product']['keywords']?>" /></td>
	  </tr>
       <tr>
		<td class="add-edit-txt">Описание:</td>
		<td><input class="head-text" type="text" name="description" value="<?=$_SESSION['add_product']['description']?>" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Родительская категория:</td>
		<td>
        <select class="select-inf" name="categories_id" multiple="" size="10" style="height: auto;">
        	<?php foreach($cat as $key_parent => $item): ?>
                <?php if(count($item) > 1): // если это родительская категория ?>
                    <option disabled=""><?=$item[0]?></option>
                    <?php $i = 0; ?>
                    <?php foreach($item['sub'] as $key => $sub): // цикл дочерних категорий ?>
                        <option <?php if($key == $categories_id OR $key_parent == $categories_id AND $i == 0) {echo "selected"; $i = 1;} ?> value="<?=$key?>">&nbsp;&nbsp;- <?=$sub?></option>
                    <?php endforeach; // конец цикла дочерних категорий ?>
                <?php elseif($item[0]): // если самостоятельная категория ?>
                    <option <?php if($key_parent == $categories_id) echo "selected" ?> value="<?=$key_parent?>"><?=$item[0]?></option>
                <?php endif; // конец условия родительская категория ?>
            <?php endforeach; ?>
        </select>
        </td>
	  </tr>
       <tr>
		<td>Картинка товара:</td>
		<td><input type="file" name="baseimg" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Количество страниц:</td>
		<td><input class="head-text" type="text" name="kol_page" value="<?=$_SESSION['add_product']['kol_page']?>" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Дата выпуска:</td>
		<td><input class="head-text" type="text" name="date_vp" value="<?=$_SESSION['add_product']['date_vp']?>" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Автор:</td>
		<td><input class="head-text" type="text" name="autor" value="<?=$_SESSION['add_product']['autor']?>" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Издатель:</td>
		<td><input class="head-text" type="text" name="izdatel" value="<?=$_SESSION['add_product']['izdatel']?>" /></td>
	  </tr>
      <tr>
        <td>Опубликовано:</td>
        <td><input type="radio" name="visible" value="1" checked="" /> Да <br />
        <input type="radio" name="visible" value="0" /> Нет</td>
      </tr> 
      <tr>
		<td>Анонс:</td>
		<td></td>
	  </tr>
	  <tr>
		<td colspan="2">
			<textarea id="editor1" class="anons-text" name="anons"><?=$_SESSION['add_product']['anons']?></textarea>
<script type="text/javascript">
	CKEDITOR.replace( 'editor1' );
</script>
		</td>
	  </tr>
      <tr>
		<td>Подробное описание:</td>
		<td></td>
	  </tr>
	  <tr>
		<td colspan="2">
			<textarea id="editor2" class="anons-text" name="text"><?=$_SESSION['add_product']['text']?></textarea>
<script type="text/javascript">
	CKEDITOR.replace( 'editor2' );
</script>
		</td>
	  </tr>
       
	</table>
	
	<input type="image" src="<?=ADMIN_TAMPLATE?>images/save.png"  /> 
</form>
<?php unset($_SESSION['add_product']); ?>	
	</div> <!-- .content -->
	</div> <!-- .content-main -->
</div> <!-- .karkas -->
</div>