<?php defined('ISHOP') or die('Access denied'); ?>
<div id="contentwrapper">
<div id="content">

<div class="grid-wrap">
<div class="catalog-index">
    <div class="content">
   <?php if(count($categories_name) > 1):?>
    <div class="croshca">
    <a href="?view=categories">Все категории</a> 
    / <a href="?view=cat&amp;categories=<?=$categories_name[0]['categories_id']?>"><?=$categories_name[0]['name']?></a> 
    / <span><?=$categories_name[1]['name']?></span>
    </div>
    <div class="content-header">
            <h1>Список категорий - <?=$categories_name[1]['name']?></h1>
    </div>
    <?php else: ?>
    <div class="croshca">
	<a href="?view=categories">Все категории</a> 
    / <span><?=$categories_name[0]['name']?></span>
    </div>
    <div class="content-header">
            <h1>Список категорий - <?=$categories_name[0]['name']?></h1>
    </div>
    <?php endif; ?>
     <?php if(isset($_SESSION['answer'])){
    echo $_SESSION['answer'];
    unset($_SESSION['answer']);
    }
    ?>

    <p class="crosh-right"><a href="?view=edit_categories&amp;categories_id=<?=$category?>&amp;parent_id=<?=$categories_name[0]['categories_id']?>" class="edit">изменить категорию</a>&nbsp; | &nbsp;<a href="?view=del_categories&amp;categories_id=<?=$category?>" class="del">удалить категорию</a></p>
    
    
        <div class="btn_content"><a href="?view=add_categories">добавить категорию</a></div>
        		
        
        <div class="btn_content"><a href="?view=add_product&amp;categories_id=<?=$category?>">добавить продукт</a></div>
        
        <?php if($products): // если есть товары?>
        
    
        <!-- Табличный вид продуктов -->
            <table class="zakaz-maiin-table" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td class="z_top">&nbsp;&nbsp;&nbsp;&nbsp;Наименование</td>
		<td class="z_top z_top_price" align="center">Цена</td>
		<td class="z_top" align="center">&nbsp;</td>
	  </tr>
    <?php foreach($products as $product): ?>
	  <tr>
		<td class="z_name">
			<a href="?view=product&amp;goods_id=<?=$product['goods_id']?>"><img src="<?=PRODUCTIMG?><?=$product['image']?>" width="32" title="" /></a> 
			<a href="?view=product&amp;goods_id=<?=$product['goods_id']?>"><?=$product['name']?></a>
            &nbsp;<?php if(!$product['visible']) echo '(Не опубликовано)';?>
		</td>
		<td class="z_price"><span><?=$product['price']?></span></td>
		<td class="z_del"><p><a href="?view=edit_product&amp;goods_id=<?=$product['goods_id']?>" class="edit">изменить</a><br /><a href="?view=del_product&amp;goods_id=<?=$product['goods_id']?>" class="del">удалить</a></p>
        </a></td>
	  </tr>
    <?php endforeach; ?>
	</table>
    <div class="clr"></div>
    <?php if($pages_count > 1) pagination($page, $pages_count); ?>
    <?php else: ?>
        <p class="error">Товаров нет!</p>
    <?php endif; ?>	
		</div>
</div>
</div>
</div>
</div>    			