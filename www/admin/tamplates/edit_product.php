<?php defined('ISHOP') or die('Access denied'); ?>
<div id="contentwrapper">
<div id="content">
	<div class="content-main">
		<div class="content">
	<h2>Редактирование товара</h2>
<?php
if(isset($_SESSION['edit_product']['res'])){
    echo $_SESSION['edit_product']['res'];
    unset($_SESSION['edit_product']);
}
?>
<div id="goods_id" style="display: none;"><?=$get_product['goods_id']?></div>
<form action="" method="post" enctype="multipart/form-data">
				
	<table class="add_edit_page" cellspacing="0" cellpadding="0">
	  <tr>
		<td class="add-edit-txt">Название товара:</td>
		<td><input class="head-text" type="text" name="name" value="<?=htmlspecialchars($get_product['name'])?>"/></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Цена:</td>
		<td><input class="head-text" type="text" name="price" value="<?=$get_product['price']?>" /></td>
	  </tr>
       <tr>
		<td class="add-edit-txt">Ключевые слова</td>
		<td><input class="head-text" type="text" name="keywords" value="<?=htmlspecialchars($get_product['keywords'])?>" /></td>
	  </tr>
       <tr>
		<td class="add-edit-txt">Описание:</td>
		<td><input class="head-text" type="text" name="description" value="<?=htmlspecialchars($get_product['description'])?>" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Родительская категория:</td>
		<td>
        <select class="select-inf" name="categories_id" multiple="" size="10" style="height: auto;">
        	<?php foreach($cat as $key_parent => $item): ?>
                <?php if(count($item) > 1): // если это родительская категория ?>
                    <option disabled=""><?=$item[0]?></option>
                    <?php $i = 0; ?>
                    <?php foreach($item['sub'] as $key => $sub): // цикл дочерних категорий ?>
                        <option <?php if($key == $categories_id OR $key_parent == $categories_id AND $i == 0) {echo "selected"; $i = 1;} ?> value="<?=$key?>">&nbsp;&nbsp;- <?=$sub?></option>
                    <?php endforeach; // конец цикла дочерних категорий ?>
                <?php elseif($item[0]): // если самостоятельная категория ?>
                    <option <?php if($key_parent == $categories_id) echo "selected" ?> value="<?=$key_parent?>"><?=$item[0]?></option>
                <?php endif; // конец условия родительская категория ?>
            <?php endforeach; ?>
        </select>
        </td>
	  </tr>
       <tr>
		<td>Картинка товара: <br />
        <span class="small">Для удаления картинки кликните по ней</span></td>
		<td class="baseimg"><?=$baseimg?></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Количество страниц:</td>
		<td><input class="head-text" type="text" name="kol_page" value="<?=$get_product['kol_page']?>" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Дата выпуска:</td>
		<td><input class="head-text" type="text" name="date_vp" value="<?=htmlspecialchars($get_product['date_vp'])?>" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Автор:</td>
		<td><input class="head-text" type="text" name="autor" value="<?=htmlspecialchars($get_product['autor'])?>" /></td>
	  </tr>
      <tr>
		<td class="add-edit-txt">Издатель:</td>
		<td><input class="head-text" type="text" name="izdatel" value="<?=htmlspecialchars($get_product['izdatel'])?>" /></td>
	  </tr>
      <tr>
        <td>Опубликовано:</td>
        <td><input type="radio" name="visible" value="1" <?php if($get_product['visible']) echo 'checked=""'; ?> /> Да <br />
        <input type="radio" name="visible" value="0" <?php if(!$get_product['visible']) echo 'checked=""'; ?> /> Нет</td>
      </tr> 
      <tr>
		<td>Анонс:</td>
		<td></td>
	  </tr>
	  <tr>
		<td colspan="2">
			<textarea id="editor1" class="anons-text" name="anons"><?=htmlspecialchars($get_product['anons'])?></textarea>
<script type="text/javascript">
	CKEDITOR.replace( 'editor1' );
</script>
		</td>
	  </tr>
      <tr>
		<td>Подробное описание:</td>
		<td></td>
	  </tr>
	  <tr>
		<td colspan="2">
			<textarea id="editor2" class="anons-text" name="text"><?=htmlspecialchars($get_product['text'])?></textarea>
<script type="text/javascript">
	CKEDITOR.replace( 'editor2' );
</script>
		</td>
	  </tr>
       
	</table>
	
	<input type="image" src="<?=ADMIN_TAMPLATE?>images/save.png"  /> 
</form>
<?php unset($_SESSION['add_product']); ?>	
	</div> <!-- .content -->
	</div> <!-- .content-main -->
</div> <!-- .karkas -->
</div>
