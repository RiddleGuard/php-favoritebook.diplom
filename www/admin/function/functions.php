<?php 
defined('ISHOP') or die('Access denied'); 
/* ====Каталог - получение массива=== */
function catalog(){
    $query = "SELECT * FROM categories ORDER BY parent_id, categories_id";
    $res = mysql_query($query) or die(mysql_query());
    
    //массив категорий
    $cat = array();
    while($row = mysql_fetch_assoc($res)){
        if(!$row['parent_id']){
            $cat[$row['categories_id']][] = $row['name'];
        }else{
            $cat[$row['parent_id']]['sub'][$row['categories_id']] = $row['name'];
        }
    }
    return $cat;
}
/* ====Каталог - получение массива=== */

/*==кол-во новостей==*/
function count_news(){
    $query = "SELECT COUNT(news_id) FROM news";
      $res = mysql_query($query)or die(mysql_error());
      $count_news = mysql_fetch_row($res);
      return $count_news[0];
}
/*==кол-во новостей==*/

/* ==добавление новости==*/
function add_news(){
    $title = trim($_POST['title']);
    $keywords = trim($_POST['keywords']);
    $description = trim($_POST['description']);
    $anons = trim($_POST['anons']);
    $text = trim($_POST['text']);
    
    if(empty($title)){
        $_SESSION['add_news']['res']="<div class='error'>Должно быть название новости</div>";
        $_SESSION['add_news']['keywords']=$keywords;
        $_SESSION['add_news']['description']=$description;
        $_SESSION['add_news']['anons']=$anons;
        $_SESSION['add_news']['text']=$text;
        return false;
    }else{
        $title = clear_admin($title);
        $keywords = clear_admin($keywords);
        $description = clear_admin($description);
        $date = date("Y-m-d");
        $anons = clear_admin($anons);
        $text = clear_admin($text);
        
        $query = "INSERT INTO news (title, keywords, description,date, anons,text)
                    values ('$title','$keywords','$description','$date', '$anons','$text')";
        $res = mysql_query($query) or die (mysql_error());
        if(mysql_affected_rows() > 0){
            $_SESSION['answer']="<div class='success'>Новость добавлена.</div>";
            return true;
        }else{
            $_SESSION['add_page']['res']="<div class='error'>Ошибка!!!</div>";
            return false;
        }
    }
}
/* ==добавление новости==*/

/* ==отдельная Новость==*/
function get_news($news_id){
    $query= "Select * 
    from news where news_id =$news_id";
    $res = mysql_query($query)or die(mysql_error());
    $news = array();
    $news = mysql_fetch_assoc($res);
    return $news;
}
/* ==отдельная Новость==*/

/*==все новости=*/
function get_all_news($start_pos,$perpage){
     $query = "SELECT news_id,title, anons, date FROM news ORDER BY date DESC,news_id DESC LIMIT $start_pos, $perpage";
      $res = mysql_query($query)or die(mysql_error());
    $all_new = array();
    while($row=mysql_fetch_assoc($res)){
        $all_new[]=$row;
    }
    return $all_new;
}
/*==все новости=*/

/* ==Редактирование новости==*/
function edit_news($news_id){
    $title = trim($_POST['title']);
    $keywords = trim($_POST['keywords']);
    $description = trim($_POST['description']);
    $date = trim($_POST['date']);
    $anons = trim($_POST['anons']);
    $text = trim($_POST['text']);
    
    if(empty($title)){
        $_SESSION['edit_news']['res']="<div class='error'>Должно быть название новости</div>";
        return false;
    }else{
        $title = clear_admin($title);
        $keywords = clear_admin($keywords);
        $description = clear_admin($description);
        $date = clear_admin($date);
        $anons = clear_admin($anons);
        $text = clear_admin($text);
        
        $query = "UPDATE news SET
                    keywords = '$keywords',
                    description = '$description',
                    title = '$title',
                    anons = '$anons',
                    text = '$text',
                    date = '$date'
                        WHERE news_id = $news_id";
        $res = mysql_query($query) or die (mysql_error());
        if(mysql_affected_rows() > 0){
            $_SESSION['answer']="<div class='success'>Всё сохранено.</div>";
            return true;
        }else{
            $_SESSION['edit_news']['res']="<div class='error'>Ошибка или вы ничего не меняли</div>";
            return false;
        }
    }
}
/* ==Редактирование новости==*/
/* ==удаление Новости==*/
function del_news($news_id){
    $query = "DELETE FROM news WHERE news_id = $news_id";
    $res = mysql_query($query) or die (mysql_error());
        if(mysql_affected_rows() > 0){
            $_SESSION['answer'] = "<div class='success'>Новость удалена</div>";
            return true;
        }else{
            $_SESSION['answer']="<div class='error'>Ошибка при удалении новости</div>";
            return false;
        }
}
/* ==удаление новости==*/

/* Добавление категории */
function add_categories(){
    $categories_name = clear_admin(trim($_POST['categories_name']));
    $parent_id = (int)$_POST['parent_id'];
    
    if(empty($categories_name)){
        $_SESSION['add_categories']['res']="<div class='error'>Должно быть название категории</div>";
        return false;
    }else{
        $query = "SELECT categories_id FROM categories 
                    WHERE name = '$categories_name' and parent_id = $parent_id";
        $res = mysql_query($query) or die (mysql_error());
        if(mysql_num_rows($res) > 0){
            $_SESSION['add_categories']['res']="<div class='error'>Категория с таким именем уже есть.</div>";
            return false;
        }else{
            $query = "INSERT INTO categories (name, parent_id)
                    values ('$categories_name',$parent_id)";
            $res = mysql_query($query) or die (mysql_error());
            if(mysql_affected_rows() > 0){
                $_SESSION['answer']="<div class='success'>Все сохранено</div>";
                return true;
            }else{
                $_SESSION['add_categories']['res']="<div class='error'>Ошибка!!!</div>";
                return false;
            }
        }
    }
}
/* Добавление категории */

/* Редактирование категории */
function edit_categories($categories_id){
    $categories_name = clear_admin(trim($_POST['cat_name']));
    $parent_id = (int)$_POST['parent_id'];
    
    if(empty($categories_name)){
        $_SESSION['edit_categories']['res']="<div class='error'>Должно быть название категории</div>";
        return false;
    }else{
        //Проверяем нет ли такой категории
        $query = "SELECT categories_id FROM categories 
                    WHERE name = '$categories_name' and parent_id = $parent_id";
            $res = mysql_query($query) or die (mysql_error());
            if(mysql_num_rows($res) > 0){
            $_SESSION['edit_categories']['res']="<div class='error'>Категория с таким именем уже есть.</div>";
            return false;
        }else{
            $query = "UPDATE categories SET
                        name = '$categories_name',
                        parent_id = $parent_id
                            WHERE categories_id = $categories_id";
            $res = mysql_query($query) or die (mysql_error());
            if(mysql_affected_rows() > 0){
                $_SESSION['answer']="<div class='success'>Все сохранено</div>";
                return true;
            }else{
                $_SESSION['edit_categories']['res']="<div class='error'>Ошибка!!!</div>";
                return false;
            }
    }
}
}
/* Редактирование категории */

/* Удаление категорий */
function del_categories($categories_id){
    $query = "SELECT COUNT(*) FROM categories 
                    WHERE parent_id = $categories_id";
            $res = mysql_query($query) or die (mysql_error());
            $row = mysql_fetch_row($res);
            if(!$row[0]){
                $query = "DELETE FROM categories WHERE categories_id = $categories_id";
                $res = mysql_query($query) or die (mysql_error());
                $query = "DELETE FROM goods WHERE categories_id = $categories_id";
                $res = mysql_query($query) or die (mysql_error());
                $_SESSION['answer'] = "<div class='success'>Категория удалена</div>";
                return true;
                
            }else{
                $_SESSION['answer']="<div class='error'>Категория имеет подкатегории</div>";
                return false;
            }
}
/* Удаление категорий */

/* ==количество товаров в каталоге== */
function count_rows($category){
    $query = "(SELECT COUNT(goods_id) as count_rows
                 FROM goods
                     WHERE categories_id = $category)
               UNION      
               (SELECT COUNT(goods_id) as count_rows
                 FROM goods 
                     WHERE categories_id IN 
                (
                    SELECT categories_id FROM categories WHERE parent_id = $category
                ) )";
    $res = mysql_query($query) or die(mysql_error());
    
    while($row = mysql_fetch_assoc($res)){
        if($row['count_rows']) $count_rows = $row['count_rows'];
    }
    return $count_rows;
}
/* ==количество товаров в каталоге== */

/* ===Получение названий для крошек=== */
function categories_name($category){
    $query="(SELECT categories_id, name FROM categories
                WHERE categories_id = 
                (SELECT parent_id FROM categories WHERE categories_id = $category))
            UNION
            (SELECT categories_id, name FROM categories WHERE categories_id = $category)";
    $res = mysql_query($query) or die(mysql_error());
    $categories_name = array();
    while($row = mysql_fetch_assoc($res)){
        $categories_name[] = $row;
    }
    return $categories_name;
}
/* ===Получение названий для крошек=== */

/* ===Получение массива товаров по категории=== */
function products($category, $start_pos, $perpage){
    $query = "(SELECT goods_id, name, image, anons, price, categories_id,date,visible
                 FROM goods
                     WHERE categories_id = $category)
               UNION      
               (SELECT goods_id, name, image, anons, price, categories_id,date,visible
                 FROM goods 
                     WHERE categories_id IN 
                (
                    SELECT categories_id FROM categories WHERE parent_id = $category
                ) )  LIMIT $start_pos, $perpage";
    $res = mysql_query($query) or die(mysql_error());
    
    $products = array();
    while($row = mysql_fetch_assoc($res)){
        $products[] = $row;
    }
    
    return $products;
}
/* ===Получение массива товаров по категории=== */

/* ===Удаление картинки=== */
function del_img(){
    $goods_id = (int)$_POST['goods_id'];
    $img = clear_admin($_POST['img']);
    
        // если удаляется базовая картинка
        $query = "UPDATE goods SET image = 'no_image.jpg' WHERE goods_id = $goods_id";
        mysql_query($query);
        if(mysql_affected_rows() > 0){
            return '<input type="file" name="baseimg" />';
        }else{
            return false;}
}
/* ===Удаление картинки=== */

/* ===Удаление товара=== */
function del_product($goods_id){
            $query = "DELETE FROM goods WHERE goods_id = $goods_id";
    $res = mysql_query($query) or die (mysql_error());
        if(mysql_affected_rows() > 0){
            $_SESSION['answer'] = "<div class='success'>Товар удален</div>";
            return true;
        }else{
            $_SESSION['answer']="<div class='error'>Ошибка при удалении товара</div>";
            return false;
        }
}
/* ===Удаление товара=== */

/* ===Редактирование товара=== */
function edit_product($id){
    $name = trim($_POST['name']);
    $price = round(floatval(preg_replace("#,#",".",$_POST['price'])),2);
    $keywords = trim($_POST['keywords']);
    $description = trim($_POST['description']);
    $categories_id = trim($_POST['categories_id']);
    $kol = (int)$_POST['kol_page'];
    $date_vp = (int)$_POST['date_vp'];
    $autor = trim($_POST['autor']);
    $izdatel = trim($_POST['izdatel']);
    $anons = trim($_POST['anons']);
    $text = trim($_POST['text']);
    $visible = (int)$_POST['visible'];
    
    if(empty($name)){
        $_SESSION['edit_product']['res'] = "<div class='error'>У товара должно быть название</div>";
        return false;
    }else{
        $name = clear_admin($name);
        $keywords = clear_admin($keywords);
        $description = clear_admin($description);
        $autor = clear_admin($autor);
        $izdatel = clear_admin($izdatel);
        $anons = clear_admin($anons);
        $text = clear_admin($text);
        
        $query = "UPDATE goods SET
                    name = '$name',
                    keywords = '$keywords',
                    description = '$description',
                    categories_id = $categories_id,
                    anons = '$anons',
                    text = '$text',
                    visible = '$visible',
                    price = $price,
                    date_vp = '$date_vp',
                    kol_page = $kol,
                    autor = '$autor',
                    izdatel ='$izdatel'
                        WHERE goods_id = $id";
        $res = mysql_query($query) or die(mysql_error());
        /* базовая картинка */
        $types = array("image/gif", "image/png", "image/jpeg", "image/pjpeg", "image/x-png"); // массив допустимых расширений
        if($_FILES['baseimg']['name']){
            $baseimgExt = strtolower(preg_replace("#.+\.([a-z]+)$#i", "$1", $_FILES['baseimg']['name'])); // расширение картинки
            $baseimgName = "{$id}.{$baseimgExt}"; // новое имя картинки
            $baseimgTmpName = $_FILES['baseimg']['tmp_name']; // временное имя файла
            $baseimgSize = $_FILES['baseimg']['size']; // вес файла
            $baseimgType = $_FILES['baseimg']['type']; // тип файла
            $baseimgError = $_FILES['baseimg']['error']; // 0 - OK, иначе - ошибка
            
            $error = "";
            if(!in_array($baseimgType, $types)) $error .= "Допустимые расширения - .gif, .jpg, .jpeg, .png <br />";
            if($baseimgSize > SIZE) $error .= "Максимальный вес файла - 1 Мб";
            if($baseimgError) $error .= "Ошибка при загрузке файла. Возможно, файл слишком большой";
            
            if(!empty($error)) $_SESSION['answer'] = "<div class='error'>Ошибка при загрузке картинки товара! <br /> {$error}</div>";
            
            // если нет ошибок
            if(empty($error)){
                if(@move_uploaded_file($baseimgTmpName, "../userfiles/temp/$baseimgName")){
                    // resize("../userfiles/temp/$baseimgName", "../userfiles/product_img/$baseimgName", 90, 129, $baseimgExt);
                        // @unlink("../userfiles/temp/$baseimgName");
                        mysql_query("UPDATE goods SET image = '$baseimgName' WHERE goods_id = $id");
                }else{
                    $_SESSION['answer'] .= "<div class='error'>Не удалось переместить загруженную картинку. Проверьте права на папки в каталоге /userfiles/product_img/</div>";
                }
            }
        }
        /* базовая картинка */
        $_SESSION['answer'] .= "<div class='success'>Товар обновлен</div>";
        return true;
    }
}
/* ===Редактирование товара=== */

/* ===Добавление товара=== */
function add_product(){
    $name = trim($_POST['name']);
    $price = round(floatval(preg_replace("#,#",".",$_POST['price'])),2);
    $keywords = trim($_POST['keywords']);
    $description = trim($_POST['description']);
    $categories_id = trim($_POST['categories_id']);
    $kol = (int)$_POST['kol_page'];
    $date_vp = (int)$_POST['date_vp'];
    $autor = trim($_POST['autor']);
    $izdatel = trim($_POST['izdatel']);
    $anons = trim($_POST['anons']);
    $text = trim($_POST['text']);
    $visible = (int)$_POST['visible'];
    $date = date("Y-m-d");
    
    if(empty($name)){
        $_SESSION['add_product']['res']="<div class='error'>Должно быть название</div>";
        $_SESSION['add_product']['price']=$price;
        $_SESSION['add_product']['keywords']=$keywords;
        $_SESSION['add_product']['description']=$description;
        $_SESSION['add_product']['kol_page']=$kol;
        $_SESSION['add_product']['date_vp']=$date_vp;
        $_SESSION['add_product']['autor']=$autor;
        $_SESSION['add_product']['izdatel']=$izdatel;
        $_SESSION['add_product']['anons']=$anons;
        $_SESSION['add_product']['text']=$text;
        return false;
    }else{
        $name = clear_admin($name);
        $keywords = clear_admin($keywords);
        $description = clear_admin($description);
        $autor = clear_admin($autor);
        $izdatel = clear_admin($izdatel);
        $anons = clear_admin($anons);
        $text = clear_admin($text);
        
        $query = "INSERT INTO goods (name,anons,text,visible,price,categories_id,date,date_vp,kol_page,autor,izdatel,keywords,description) 
                        VALUES ('$name','$anons','$text','$visible',$price,$categories_id,'$date','$date_vp',$kol,'$autor','$izdatel','$keywords','$description')";
        $res = mysql_query($query) or die (mysql_error());
        
        if(mysql_affected_rows() > 0){
            $id = mysql_insert_id(); // ID сохраненного товара
            $types = array("image/gif", "image/png", "image/jpeg", "image/pjpeg", "image/x-png"); // массив допустимых расширений
            /* базовая картинка */
            if($_FILES['baseimg']['name']){
                $baseimgExt = strtolower(preg_replace("#.+\.([a-z]+)$#i", "$1", $_FILES['baseimg']['name'])); // расширение картинки
                $baseimgName = "{$id}.{$baseimgExt}"; // новое имя картинки
                $baseimgTmpName = $_FILES['baseimg']['tmp_name']; // временное имя файла
                $baseimgSize = $_FILES['baseimg']['size']; // вес файла
                $baseimgType = $_FILES['baseimg']['type']; // тип файла
                $baseimgError = $_FILES['baseimg']['error']; // 0 - OK, иначе - ошибка
                $error = "";
                
                if(!in_array($baseimgType, $types)) $error .= "Допустимые расширения - .gif, .jpeg, .jpg, .png <br />";
                if($baseimgSize > SIZE) $error .= "Максимальный вес файла - 1 Мб";
                if($baseimgError) $error .= "Ошибка при загрузке файла. Возможно, файл слишком большой";
                
                if(!empty($error)) $_SESSION['answer'] = "<div class='error'>Ошибка при загрузке картинки товара! <br /> {$error}</div>";
                
                // если нет ошибок
                if(empty($error)){
                    if(@move_uploaded_file($baseimgTmpName, "../userfiles/temp/$baseimgName")){
                        // resize("../userfiles/temp/$baseimgName", "../userfiles/product_img/$baseimgName", 90, 129, $baseimgExt);
                        // @unlink("../userfiles/temp/$baseimgName");
                        mysql_query("UPDATE goods SET image = '$baseimgName' WHERE goods_id = $id");
                    }else{
                        $_SESSION['answer'] .= "<div class='error'>Не удалось переместить загруженную картинку. Проверьте права на папки в каталоге /userfiles/product_img/</div>";
                    }
                }
            }
            $_SESSION['answer'] .= "<div class='success'>Товар добавлен</div>";
            return true;
        }else{
            $_SESSION['add_product']['res']="<div class='error'>Ошибка при добавлении товара.</div>";
            return false;
        }
    }
}
/* ===Добавление товара=== */

/* ===Получение данных товара=== */
function get_product($goods_id){
    $query = "SELECT * FROM goods WHERE goods_id = $goods_id";
    $res = mysql_query($query)or die (mysql_error());
    
    $product = array();
    $product = mysql_fetch_assoc($res);
    return $product;
}
/* ===Получение данных товара=== */

/* фильтрация входящих данных для админки */
function clear_admin($var){
    $var = mysql_real_escape_string($var);
    return $var;
}
/* фильтрация входящих данных для админки */

/* ===Ресайз картинок=== */
function resize($target, $dest, $wmax, $hmax, $ext){
    /*
    $target - путь к оригинальному файлу
    $dest - путь сохранения обработанного файла
    $wmax - максимальная ширина
    $hmax - максимальная высота
    $ext - расширение файла
    */
    list($w_orig, $h_orig) = getimagesize($target);
    $ratio = $w_orig / $h_orig; // =1 - квадрат, <1 - альбомная, >1 - книжная

    if(($wmax / $hmax) > $ratio){
        $wmax = $hmax * $ratio;
    }else{
        $hmax = $wmax / $ratio;
    }
    
    $img = "";
    // imagecreatefromjpeg | imagecreatefromgif | imagecreatefrompng
    switch($ext){
        case("gif"):
            $img = imagecreatefromgif($target);
            break;
        case("png"):
            $img = imagecreatefrompng($target);
            break;
        default:
            $img = imagecreatefromjpeg($target);    
    }
    $newImg = imagecreatetruecolor($wmax, $hmax); // создаем оболочку для новой картинки
    
    if($ext == "png"){
        imagesavealpha($newImg, true); // сохранение альфа канала
        $transPng = imagecolorallocatealpha($newImg,0,0,0,127); // добавляем прозрачность
        imagefill($newImg, 0, 0, $transPng); // заливка  
    }
    
    imagecopyresampled($newImg, $img, 0, 0, 0, 0, $wmax, $hmax, $w_orig, $h_orig); // копируем и ресайзим изображение
    switch($ext){
        case("gif"):
            imagegif($newImg, $dest);
            break;
        case("png"):
            imagepng($newImg, $dest);
            break;
        default:
            imagejpeg($newImg, $dest);    
    }
    imagedestroy($newImg);
}
/* ===Ресайз картинок=== */

/* ==Страницы имена==*/
function pages(){
    $query= "Select page_id,title,position from pages ORDER BY position";
    $res = mysql_query($query)or die(mysql_error());
    $pages = array();
    while($row=mysql_fetch_assoc($res)){
        $pages[]=$row;
    }
    return $pages;
}
/* ==Страницы имена==*/

/* ==отдельная страница==*/
function get_page($page_id){
    $query= "Select * 
    from pages where page_id =$page_id";
    $res = mysql_query($query)or die(mysql_error());
    $page = array();
    $page = mysql_fetch_assoc($res);
    return $page;
}
/* ==отдельная страница==*/

/* ==добавление странницы==*/
function add_page(){
    $title = trim($_POST['title']);
    $keywords = trim($_POST['keywords']);
    $description = trim($_POST['description']);
    $position = (int)$_POST['position'];
    $text = trim($_POST['text']);
    
    if(empty($title)){
        $_SESSION['add_page']['res']="<div class='error'>Должно быть название страницы</div>";
        $_SESSION['add_page']['keywords']=$keywords;
        $_SESSION['add_page']['description']=$description;
        $_SESSION['add_page']['position']=$position;
        $_SESSION['add_page']['text']=$text;
        return false;
    }else{
        $title = clear_admin($title);
        $keywords = clear_admin($keywords);
        $description = clear_admin($description);
        $text = clear_admin($text);
        
        $query = "INSERT INTO pages (title, keywords, description,position,text)
                    values ('$title','$keywords','$description','$position','$text')";
        $res = mysql_query($query) or die (mysql_error());
        if(mysql_affected_rows() > 0){
            $_SESSION['answer']="<div class='success'>Страница добавлена.</div>";
            return true;
        }else{
            $_SESSION['add_page']['res']="<div class='error'>Ошибка!!!</div>";
            return false;
        }
    }
}
/* ==добавление странницы==*/
/* ==удаление странницы==*/
function del_page($page_id){
    $query = "DELETE FROM pages WHERE page_id = $page_id";
    $res = mysql_query($query) or die (mysql_error());
        if(mysql_affected_rows() > 0){
            $_SESSION['answer'] = "<div class='success'>Страница удалена</div>";
            return true;
        }else{
            $_SESSION['answer']="<div class='error'>Ошибка при удалении страницы</div>";
            return false;
        }
}
/* ==удаление странницы==*/
/* ==Редактирование странницы==*/
function edit_page($page_id){
    $title = trim($_POST['title']);
    $keywords = trim($_POST['keywords']);
    $description = trim($_POST['description']);
    $position = (int)$_POST['position'];
    $text = trim($_POST['text']);
    
    if(empty($title)){
        $_SESSION['edit_page']['res']="<div class='error'>Должно быть название страницы</div>";
        return false;
    }else{
        $title = clear_admin($title);
        $keywords = clear_admin($keywords);
        $description = clear_admin($description);
        $text = clear_admin($text);
        
        $query = "UPDATE pages SET
                    title = '$title',
                    keywords = '$keywords',
                    description = '$description',
                    position = '$position',
                    text = '$text'
                        WHERE page_id = $page_id";
        $res = mysql_query($query) or die (mysql_error());
        if(mysql_affected_rows() > 0){
            $_SESSION['answer']="<div class='success'>Всё сохранено.</div>";
            return true;
        }else{
            $_SESSION['edit_page']['res']="<div class='error'>Ошибка или вы ничего не меняли</div>";
            return false;
        }
    }
}
/* ==Редактирование странницы==*/

/* получение количества необработанных заказов */
function count_orders(){
    $query = "SELECT COUNT(*) AS count From orders WHERE status = '0'";
    $res = mysql_query($query);
    $row = mysql_fetch_assoc($res);
    return $row['count'];
}
/* получение количества необработанных заказов */

/* Получение необработаных заказов*/
function orders($start_pos, $perpage,$status){
    $query = "SELECT orders.order_id, orders.date, users.name, orders.status
                FROM orders
                LEFT JOIN users
                    ON users.user_id = orders.user_id
                    ".$status." ORDER BY orders.date DESC LIMIT $start_pos, $perpage";
    $res = mysql_query($query);
    $new_orders = array();
    while($row = mysql_fetch_assoc($res)){
        $new_orders[] = $row;
    }
    return $new_orders;
}
/* Получение необработаных заказов*/

/* Просмотр заказа */
function show_order($order_id){
    //orders,order_goods,users,dostavka
    $query = "SELECT order_goods.name, order_goods.price, order_goods.quantity,
                    orders.date, orders.prim,orders.status,
                    users.name as username, users.email, users.phone, users.address,
                    dostavka.name as dostavkaname
                        FROM order_goods
                        LEFT JOIN orders
                            ON order_goods.order_id=orders.order_id 
                        LEFT JOIN users
                            ON users.user_id=orders.user_id
                        LEFT JOIN dostavka
                            ON dostavka.dostavka_id=orders.dostavka_id
                                Where order_goods.order_id = $order_id";
    $res = mysql_query($query);
    $show_order =array();
    while($row = mysql_fetch_assoc($res)){
        $show_order[]=$row;
    }
    return $show_order;
}
/* Просмотр заказа */

function confirm_order($order_id){
    $query = "UPDATE orders SET status = '1' WHERE order_id=$order_id";
    $res = mysql_query($query);
    if(mysql_affected_rows()> 0){
        return true;
    }else{
        return false;
    }
}

/* ==удаление заказа==*/
function del_order($order_id){
    $query = "DELETE FROM orders WHERE order_id=$order_id";
    $query2 ="DELETE FROM order_goods WHERE order_id=$order_id";
    $res = (mysql_query($query) and mysql_query($query2)) or die (mysql_error());
        if(mysql_affected_rows() > 0){
            $_SESSION['answer'] = "<div class='success'>Заказ удален</div>";
            return true;
        }else{
            $_SESSION['answer']="<div class='error'>Ошибка при удалении заказа</div>";
            return false;
        }
}
/* ==удаление заказа==*/
?>