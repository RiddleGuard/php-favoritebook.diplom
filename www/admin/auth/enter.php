<?php
define('ISHOP', TRUE);
session_start();

include $_SERVER['DOCUMENT_ROOT'].'/config.php';

if($_SESSION['auth']['admin']){
    header("Location: ../");
    exit;
}

if($_POST){
    $login = trim(mysql_real_escape_string($_POST['login']));
    $pass = trim($_POST['pass']);
    $query = "SELECT user_id, name, email, login, password FROM users WHERE login = '$login' AND id_role = 2 LIMIT 1";
    $res = mysql_query($query);
    $row = mysql_fetch_assoc($res);
    if($row['password'] == md5($pass)){
        $_SESSION['auth']['admin'] = htmlspecialchars($row['login']);
        $_SESSION['auth']['user_id'] = $row['user_id'];
        $_SESSION['auth']['user'] = $row['name'];
        $_SESSION['auth']['email'] = $row['email'];
        $_SESSION['auth']['login'] = $row['login'];
        header("Location: ../");
        exit;
    }else{
        $_SESSION['res'] = '<div class="error">Логин или пароль не совпадает!</div>';
        header("Location: {$_SERVER['PHP_SELF']}");
        exit;
    }
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="../<?=ADMIN_TAMPLATE?>css/style.css">
<title>Административная панель</title>
</head>
<body>
<div class="main">

	<div class="header">
        <a href="<?=PATH?>/Admin"><img class="logo" src="../<?=ADMIN_TAMPLATE?>images/logo.png" alt="Favorite book" /></a>
        <p class="title_admin">Административная панель</p>
    </div>
    <div class="header2">
    
    <div class="enter"><b><a href="<?=PATH?>">На сайт</a> </b></div>
    </div>
<div id="contentwrapper">
<div id="content">
<form method="post" action="">
<?php if(isset($_SESSION['res'])){
    echo $_SESSION['res'];
    unset($_SESSION['res']);
}
?>
                    <table class="zakaz-data" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="zakaz-txt">Логин:</td>
                            <td class="zakaz-inpt"><input type="text" name="login" value="<?=$_SESSION['reg']['login']?>" required></td>
                            <td class="zakaz-prim"></td>
                        </tr>
                        <tr>
                            <td class="zakaz-txt">Пароль:</td>
                            <td class="zakaz-inpt"><input type="password" name="pass" id="pass" required></td>
                            <td class="zakaz-prim"></td>
                        </tr>
                </table>
                <input type="submit" name="auto" value="Войти" />
            </form>
                </div><div class="clr"></div>
    <div class="footer">
    <p>Copyright © 2015 | Все права защищены.</p>
    </div>      
</div>
</body>
</html>