<?php defined('ISHOP') or die('Access denied'); ?>
<div class="croshca">
    <a href="<?=PATH?>">Гглавная</a> / <span>Оформление заказа</span>
</div>
<div class="content-header">
    <h1>Оформление заказа</h1>
</div>
<div id="content-zakaz">
    <?php if(isset($_SESSION['order']['res'])) {
    echo $_SESSION['order']['res'];  }  ?>
 <?php if($_SESSION['cart']): // проверка корзины, если в корзине есть товары ?>
	
	<form method="post" action="">
    <table class="zakaz-maiin-table" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td class="z_top">&nbsp;&nbsp;&nbsp;&nbsp;Наименование</td>
		<td class="z_top z_top_kol" align="center">Количество</td>
		<td class="z_top z_top_price" align="center">Цена</td>
		<td class="z_top" align="center">&nbsp;</td>
	  </tr>
<?php foreach($_SESSION['cart'] as $key => $item): ?>
	  <tr>
		<td class="z_name">
			<a href="?view=product&amp;goods_id=<?=$key?>"><img src="<?=PRODUCTIMG?><?=$item['image']?>" width="32" title="" /></a> 
			<a href="?view=product&amp;goods_id=<?=$key?>"><?=$item['name']?></a>
		</td>
		<td class="z_kol"><input id="id<?=$key?>" class="kolvo" type="text" value="<?=$item['qty']?>" name="" /></td>
		<td class="z_price"><?=$item['price']*$item['qty']?><br /><span><?=$item['qty']?>x<?=$item['price']?></span></td>
		<td class="z_del"><a href="?view=cart&amp;delete=<?=$key?>"><img src="<?=TEMPLATE?>images/delete.jpg" title="удалить товар из заказа" /></a></td>
	  </tr>
<?php endforeach; ?>
	  <tr>
		<td class="z_bot">&nbsp;&nbsp;&nbsp;&nbsp;Итого:</td>
		<td class="z_bot z_top_price" colspan="3" align="right"><span><?=$_SESSION['total_quantity']?></span> шт &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span><?=$_SESSION['total_sum']?></span> руб.</td>
	  </tr>
	  
	</table>
	
	<div class="sposob-dostavki">
		<h4>Способы доставки:</h4>
        <?php foreach($dostavka as $item): ?>
        <p><input type="radio" name="dostavka" value="<?=$item['dostavka_id']?>" /><?=$item['name']?></p>
        <?php endforeach; ?>
<!--			<p><input type="radio" name="1" value="1" /> Курьером, 200 руб</p> 
			<p><input type="radio" name="1" value="2" /> Самовывоз, бесплатно</p> 
			<p><input type="radio" name="1" value="3" /> В магазин, бесплатно</p> 
-->
	</div>		
	
	
	<h3>Информация для доставки:</h3>
	
	<table class="zakaz-data" border="0" cellspacing="0" cellpadding="0">
<?php if(!$_SESSION['auth']['user']): //проверка авторизации?>
      
	  <tr class="notauth">
		<td class="zakaz-txt">*ФИО</td>
		<td class="zakaz-inpt"><input type="text" name="name" value="<?=htmlspecialchars($_SESSION['order']['name'])?>" required></td>
		<td class="zakaz-prim">Пример: Иванов Сергей Александрович</td>
	  </tr>
	  <tr class="notauth">
		<td class="zakaz-txt">*Е-маил</td>
		<td class="zakaz-inpt"><input type="text" name="email" value="<?=htmlspecialchars($_SESSION['order']['email'])?>" required></td>
		<td class="zakaz-prim">Пример: test@mail.ru</td>
	  </tr>
	  <tr class="notauth">
		<td class="zakaz-txt">*Телефон</td>
		<td class="zakaz-inpt"><input type="text" name="phone" value="<?=htmlspecialchars($_SESSION['order']['phone'])?>" required></td>
		<td class="zakaz-prim">Пример: 8 937 999 99 99</td>
	  </tr>
	  <tr class="notauth">
		<td class="zakaz-txt">*Адрес доставки</td>
		<td class="zakaz-inpt"><input type="text" name="address" value="<?=htmlspecialchars($_SESSION['order']['address'])?>" required></td>
		<td class="zakaz-prim">Пример: г. Москва, пр. Мира, ул. Петра Великого д.19, кв 51.</td>
	  </tr>
      
<?php endif; ?>
	  <tr>
		<td class="zakaz-txt" style="vertical-align:top;">Примечание </td>
		<td class="zakaz-txtarea"><textarea name="prim" ><?=htmlspecialchars($_SESSION['order']['prim'])?></textarea></td>
		<td class="zakaz-prim" style="vertical-align:top;">Пример: Позвоните пожалуйста после 10 вечера, 
до этого времени я на работе </td>
	  </tr>
	</table>
		<input type="image" name="order" src="<?=TEMPLATE?>images/zakazat.png"/> 
		
		<br /><br /><br /><br />

	
	</form>
    <?php else: // если товаров нет ?>
        Корзина пуста
    <?php endif; // конец условия проверки корзины ?>
    <?php 
unset($_SESSION['order']);

 ?>	
</div> <!-- .content-zakaz -->