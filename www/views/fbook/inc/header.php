	<?php defined('ISHOP') or die('Access denied'); ?>
    <div class="header">
        <a href="/"><img class="logo" src="<?=TEMPLATE?>images/logo.png" alt="Favorite book" /></a>
        <form method="get">
        <ul class="search-head">
            <input type="hidden" name="view" value="search"/>
        	<li>
            <input type="text" name="search" id="graytext" placeholder="Что вы хотите купить?" />
        		<script type="text/javascript">
                //<![CDATA[
                	placeholderSetup('graytext');
                //]]>
                </script>
            </li>
             <li><input type="image" class="serch-btn" src="<?=TEMPLATE?>images/btn_find.png" /></li>
        </ul>
        </form>
    </div>
    <div class="header2">
    <ul class="menu">
    	<a href="<?=PATH?>"><li>Главная</li></a>
        <?php if($pages): ?>
            <?php foreach($pages as $item): ?>
                <a href="?view=page&amp;page_id=<?=$item['page_id']?>"><li><?=$item['title']?></li></a>
            <?php endforeach; ?>
        <?php endif; ?>
        <a href="?view=archive-nwes"><li>Новости</li></a>
        <!--<a href="#"><li style="color: #C10101;">Новинки</li></a>-->			       			         		           
    </ul>
    <div class="enter">
    <?php if(!$_SESSION['auth']['login']): ?>
    <b><a href="?view=reg">Регистрация</a> | <a href="?view=autorization">Вход</a></b>
    <?php else: ?>
        <p><b>
        <?php if($_SESSION['auth']['admin']): ?>
        <a href="<?=PATH?>/admin">Административная панель</a> |
        <?php endif; ?>
        <a href="#"><?=$_SESSION['auth']['login']?></a> | <a href="?do=logout">Выход</a></b></p>
    <?php endif; ?>
    </div>
    </div>