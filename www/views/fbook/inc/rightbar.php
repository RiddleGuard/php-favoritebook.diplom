<?php defined('ISHOP') or die('Access denied'); ?>
  <div id="right-bar">
        <div class="right-bar-cont">
            <h2>Выберите жанр</h2>
            <ul class="nav-catalog" id="accordion">
                <?php foreach($cat as $key => $item): ?>
                        <?php if(count($item) > 1): // если это родительская категория ?>
                        <h3><li><a href="#"><?=$item[0]?></a></li></h3>
                            <ul>
                                <li>- <a href="?view=catalog&category=<?=$key?>">Все</a></li>
                                <?php foreach($item['sub'] as $key => $sub): ?>
                                <li>- <a href="?view=catalog&category=<?=$key?>"><?=$sub?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php elseif($item[0]): // если самостоятельная категория ?>
                            <li><a href="?view=catalog&category=<?=$key?>"><?=$item[0]?></a></li>
                        <?php endif; ?>
                    <?php endforeach; ?>
            </ul>   
        </div>
        <div class="basket">
            <h2>Корзина</h2>
            <div>
                <p>
                <?php if($_SESSION['total_quantity']): ?>
                    У вас в корзин <span><?=$_SESSION['total_quantity']?></span> товар на<br />
                    Сумму <span><?=$_SESSION['total_sum']?></span> руб.
                    <div class="btn_right_bar"><a href="?view=cart">Оформить</a></div>
                <?php else: ?>
                    Корзина пуста                          
                <?php endif; ?>
                </p>
            </div>
        </div>
        <div class="right-bar-news">
            <h2>Новости</h2>
            <div>
                <p>
                    <?php if($news): ?>
                        <?php foreach($news as $item): ?>
                            <p>
        					   <a href="?view=news&amp;news_id=<?=$item['news_id']?>"><?=$item['title']?></a><br />
                               <span class="news-date"><?=$item['date']?></span>	
        					</p>   
                        <?php endforeach; ?>
                        <div class="btn_right_bar"><a href="?view=archive-nwes" class="news-arh">Все новости</a></div>
                    <?php else: ?>
                        <p>Новостей нет.</p>
                    <?php endif; ?>
                </p>
            </div>
        </div>
  </div>