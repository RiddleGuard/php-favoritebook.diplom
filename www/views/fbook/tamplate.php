
<?php defined('ISHOP') or die('Access denied'); ?>
<div class="grid-wrap">
    <div class="catalog-index">
    
        <div class="content-header">
            <h1>Все товары</h1>
        </div>
        <?php if($all_goods): ?>
            <div class="vid-sort">
            Вид: 
                <a href="#" id="grid" class="grid_list"><img src="<?=TEMPLATE?>images/view-tabel-act.png" alt="табличный вид" /></a> 
                <a href="#" id="list" class="grid_list"><img src="<?=TEMPLATE?>images/view-line-act.png" alt="линейный вид" /></a>  
                &nbsp;&nbsp;           
                Сортировать по:&nbsp;   
                <a id="param_order" class="sort-top"><?=$order?></a>
                <div class="sort-wrap">
                    <?php foreach($order_p as $key => $value): ?>
                    <?php if($value[0] == $order) continue; ?>
                    <a href="?view=tamplate&amp;order=<?=$key?>&amp;page=<?=$page?>" class="sort-bot"><?=$value[0]?></a>                        
                    <?php endforeach; ?>
                </div> 
            </div> 
            <?php foreach($all_goods as $item): ?>
            <?php if(!isset($_COOKIE['display']) OR $_COOKIE['display'] == 'grid'): // если вид - сетка ?>
            <div class="product-tabel">
            <div class="product-index bp1-col-one-half bp2-col-one-third bp3-col-one-third">
                <div class="info-goods">
                    <a href="?view=product&amp;goods_id=<?=$item['goods_id']?>"><img  src="<?=PRODUCTIMG?><?=$item['image']?>" width="90"  alt=""/></a>
                    <h2><a href="?view=product&amp;goods_id=<?=$item['goods_id']?>"><?=$item['name']?></a></h2>
                </div>
                <div class="block_addtocart">
                    <p>Цена: <span><?=$item['price']?></span> руб.</p>
                    <div class="btn_content"><a href="?view=addtocart&amp;goods_id=<?=$item['goods_id']?>">В корзину</a></div>
                </div>
            </div>
            
            </div>
            <?php else: // если линейный вид ?>
            <div class="product-line">
                <div class="product-line-har">
                    <a href="?view=product&amp;goods_id=<?=$item['goods_id']?>"><img src="<?=PRODUCTIMG?><?=$item['image']?>" width="48" alt="" /></a>
                    <p>Цена :  <span><?=$item['price']?></span></p>
                    <div class="btn_content"><a href="?view=addtocart&amp;goods_id=<?=$item['goods_id']?>">В корзину</a></div>
                    <p class="cat-line-more"><a href="?view=product&amp;goods_id=<?=$item['goods_id']?>">подробнее...</a></p>
                </div>	
                <div class="product-line-opis">
                    <h2 class="product-name"><a href="?view=product&amp;goods_id=<?=$item['goods_id']?>"><?=$item['name']?></a></h2>
                    <p><?=$item['anons']?></p>
                </div>
            </div>
            <?php endif; // конец условия переключателя видов  ?>	
            <?php endforeach; ?>
        <div class="clr"></div>
        <?php if($pages_count > 1) pagination($page, $pages_count); ?>
        <?php else: ?>
            <p>Товаров нет!</p>
        <?php endif; ?>	
    </div>
</div>	