$(document).ready(function(){
    
    /* ===Аккордеон=== */
    var openItem = false;
	if(jQuery.cookie("openItem") && jQuery.cookie("openItem") != 'false'){
		openItem = parseInt(jQuery.cookie("openItem"));
	}	
	jQuery("#accordion").accordion({
		active: openItem,
		collapsible: true,
        autoHeight: false,
        header: 'h3'
	});
	jQuery("#accordion h3").click(function(){
		jQuery.cookie("openItem", jQuery("#accordion").accordion("option", "active"));
	});	
	jQuery("#accordion > li").click(function(){
		jQuery.cookie("openItem", null);
        var link = jQuery(this).find('a').attr('href');
        window.location = link;
	});
    /* ===Аккордеон=== */
    
    /* ===Переключатель вида=== */
    if($.cookie("display") == null){
        $.cookie("display", "grid");
    }
    
    $(".grid_list").click(function(){
        var display = $(this).attr("id"); // получаем значение переключателя вида
        display = (display == "list") ? "list" : "grid" ; // допустимые значения
        if(display == $.cookie("display")){
            // если значение совпадает с кукой - ничего не делаем
            return false;   
        }else{
            // иначе - установим куку с новым значением вида
            $.cookie("display", display);
            window.location = "?" + query;
            return false;
        }
    });
    $(".grid_list").ready(function(){
        var display = $(this).attr("id"); // получаем значение переключателя вида
        display = (display == "list") ? "list" : "grid" ; // допустимые значения
        if(display == $.cookie("display")){
            // если значение совпадает с кукой - ничего не делаем
            $("#grid img").css({'background-Color':'#CFCFE8'});
            }else{
            $("#list img").css({'background-Color':'#CFCFE8'});
            }
    });
    /* ===Переключатель вида=== */
    /*==Сортировка== */
        $("#param_order").toggle(
            function(){
                $(".sort-wrap").css({'visibility':'visible'});
                
            },
            function(){
                $(".sort-wrap").css({'visibility':'hidden'});
            }
        );
    /*==Сортировка== */
    /* ===Клавиша ENTER при пересчете=== */
    $(".kolvo").keypress(function(e){
        if(e.which == 13){
            return false;
        }
    });
    /* ===Клавиша ENTER при пересчете=== */
    
    /* ===Пересчет товаров в корзине=== */
    $(".kolvo").each(function(){
       var qty_start = $(this).val(); // кол-во до изменения
       
       $(this).change(function(){
           var qty = $(this).val(); // кол-во перед пересчетом
           var res = confirm("Пересчитать корзину?");
           if(res){
                var id = $(this).attr("id");
                id = id.substr(2);
                if(!parseInt(qty)){
                    qty = qty_start;
                }
                // передаем параметры
                window.location = "?view=cart&qty=" + qty + "&id=" + id;
           }else{
                // если отменен пересчет корзины
                $(this).val(qty_start);
           }
       }); 
    });
    /* ===Пересчет товаров в корзине=== */

    
});