<?php defined('ISHOP') or die('Access denied'); ?>
<div class="croshca">
    <a href="<?=PATH?>">Гглавная</a> / <span>Новости</span>
</div>
<div class="content-header">
    <h1>Новости</h1>
</div>
<?php if($all_news): ?>
    <?php foreach($all_news as $item): ?>
    <div class="page-news-txt">
    <h1><a href="?view=news&amp;news_id=<?=$item['news_id']?>"><?=$item['title']?></a></h1>
    <span class="news-date"><?=$item['date']?></span>
    <p><?=$item['anons']?></p>
    <p class="end_news"><a href="?view=news&amp;news_id=<?=$item['news_id']?>">Подробнее...</a></p>
    </div>
    <?php endforeach; ?>
    <div class="clr"></div>
    <?php if($pages_count > 1) pagination($page, $pages_count); ?>

<?php else: ?>
    <p>такой новости нет</p>
<?php endif; ?>
