<?php
defined('ISHOP') or die('Access denied');
?>
<div class="catalog-index"> 
<div class="content-header">
            <h1>Поиск</h1>
    </div>  
    <div class="vid-sort">
    Вид: 
        <a href="#" id="grid" class="grid_list"><img src="<?=TEMPLATE?>images/view-tabel-act.png" alt="табличный вид" /></a> 
        <a href="#" id="list" class="grid_list"><img src="<?=TEMPLATE?>images/view-line-act.png" alt="линейный вид" /></a>  
        &nbsp;&nbsp;           
        Сортировать по:&nbsp;   
        <a id="param_order" class="sort-top"><?=$order?></a>
        <div class="sort-wrap">
            <?php foreach($order_p as $key => $value): ?>
            <?php if($value[0] == $order) continue; ?>
            <a href="?view=search&amp;search=<?=$_GET['search']?>&amp;order=<?=$key?>&amp;page=<?=$page?>" class="sort-bot"><?=$value[0]?></a>                        
            <?php endforeach; ?>
        </div> 
    </div> 
    <?php if($result_search['notfound']): // если ничего не найдено ?>
        <?php echo $result_search['notfound'] ?>
    <?php else: // если есть результаты поиска ?>
<?php for($i = $start_pos; $i < $endpos; $i++): ?>
<?php if(!isset($_COOKIE['display']) OR $_COOKIE['display'] == 'grid'): // если вид - сетка ?>
<!-- Табличный вид продуктов -->				
<div class="product-index bp1-col-one-half bp2-col-one-third bp3-col-one-third">
            <div class="info-goods">
                <a href="?view=product&amp;goods_id=<?=$result_search[$i]['goods_id']?>"><img src="<?=PRODUCTIMG?><?=$result_search[$i]['image']?>" alt="" height="121" /></a>
                <h2><a href="?view=product&amp;goods_id=<?=$result_search[$i]['goods_id']?>"><?=$result_search[$i]['name']?></a></h2>
            </div>
            <p>Цена :  <span><?=$result_search[$i]['price']?></span> руб.</p>
            <div class="btn_content"><a href="?view=addtocart&amp;goods_id=<?=$result_search[$i]['goods_id']?>">В корзину</a></div>
        </div> <!-- .product-table -->
<!-- Табличный вид продуктов -->
<?php else: // если линейный вид ?>
            <div class="product-line">
                <div class="product-line-har">
                    <a href="?view=product&amp;goods_id=<?=$result_search[$i]['goods_id']?>"><img src="<?=PRODUCTIMG?><?=$result_search[$i]['image']?>" alt="" /></a>
                    <p>Цена :  <span><?=$result_search[$i]['price']?></span></p>
                    <div class="btn_content"><a href="?view=addtocart&amp;goods_id=<?=$result_search[$i]['goods_id']?>">В корзину</a></div>
                    <p class="cat-line-more"><a href="?view=product&amp;goods_id=<?=$result_search[$i]['goods_id']?>">подробнее...</a></p>
                </div>	
                <div class="product-line-opis">
                    <h2 class="product-name"><a href="?view=product&amp;goods_id=<?=$result_search[$i]['goods_id']?>"><?=$result_search[$i]['name']?></a></h2>
                    <p><?=$result_search[$i]['anons']?></p>
                </div>
            </div>
            <?php endif; // конец условия переключателя видов  ?>
<?php endfor; ?>
<div class="clr"></div>
<?php if($pages_count > 1) pagination($page, $pages_count); ?>
<?php endif; ?>
    
</div>