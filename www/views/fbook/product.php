<?php defined('ISHOP') or die('Access denied'); ?>
<?php if($goods):?>
    <div class="croshca">
        <?php if(count($categories_name)>1): ?>
                <a href="<?=PATH?>">Гглавная</a> /
                 <a href="?view=catalog&amp;category=<?=$categories_name[0]['categories_id']?>"><?=$categories_name[0]['name']?></a> /
                  <a href="?view=catalog&amp;category=<?=$categories_name[1]['categories_id']?>"><?=$categories_name[1]['name']?></a> /
                  <span><?=$goods['name']?></span>
        <?php elseif(count($categories_name)==1): ?>
                <a href="<?=PATH?>">Гглавная</a> /
                 <a href="?view=catalog&amp;category=<?=$categories_name[0]['categories_id']?>"><?=$categories_name[0]['name']?></a> /
                  <span><?=$goods['name']?></span>
        <?php endif; ?>
    </div>
    <div class="content-header">
        <h1><?=$goods['name']?></h1>
    </div>
    <div class="catalog-detil">
        <img  src="<?=PRODUCTIMG?><?=$goods['image']?>" class="har-img" height="250" alt=""/>
        <p class="har">
        Характеристики:<br />
        Издательство: <span><?=$goods['izdatel']?></span><br />
        Год выпуска: <span><?=$goods['date_vp']?></span><br />
        Кол. страниц: <span><?=$goods['kol_page']?></span><br />
        Автор: <span><?=$goods['autor']?></span><br />
        </p>
        <p class="text-detil-goods">
        <?=$goods['text']?>
        </p>
        <br />
        <div class="news-date"><?=$goods['date']?></div>
        <p>Цена: <span><?=$goods['price']?></span> руб.</p>    
    </div>
    <div class="btn_content"><a href="?view=addtocart&amp;goods_id=<?=$goods['goods_id']?>">В корзину</a></div>
    
<?php else: ?>
    <p class="error">Такого товара нет</p>
<?php endif; ?>