<?php defined('ISHOP') or die('Access denied'); ?>
<!DOCTYPE html>
<!--[if lt IE 8]><html class="oldie lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if IE 8]><html class="ie8 lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html lang="ru"><!--<![endif]-->
<head>
<meta charset="utf-8">
<script type="text/javascript" src="<?=TEMPLATE?>js/functions.js"></script>
<!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<link rel="stylesheet" href="<?=TEMPLATE?>css/ie.css">
<![endif]-->
<link rel="stylesheet" href="<?=TEMPLATE?>css/style.css">
<script type="text/javascript" src="<?=TEMPLATE?>js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE?>js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="<?=TEMPLATE?>js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?=TEMPLATE?>js/workscripts.js"></script>
<script type="text/javascript"> var query = '<?=$_SERVER['QUERY_STRING']?>';</script>
<title><?=TITLE?></title>
</head>
<body>
<div class="main">
<?php require_once 'inc/header.php' ?>
	<div id="contentwrapper">
		<div id="content">
        <?php include $view. '.php' ?>
		</div>
	</div>

		<?php require_once 'inc/rightbar.php' ?>
	<div class="clr"></div>	
		<?php require_once 'inc/footer.php' ?>
</div>
</body>
</html>