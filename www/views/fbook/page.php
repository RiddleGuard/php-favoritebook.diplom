<?php defined('ISHOP') or die('Access denied'); ?>
<div class="croshca">
    <a href="<?=PATH?>">Гглавная</a> / <span><?=$get_page['title']?></span>
</div>
<?php if($get_page): ?>

<div class="content-header">
    <h1><?=$get_page['title']?></h1>
</div>
<div class="catalog-detil">
    <p><?=$get_page['text']?></p>
</div>
<?php else: ?>
    <p class="error">Станицы нет</p>
<?php endif; ?>
