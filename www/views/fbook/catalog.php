<?php defined('ISHOP') or die('Access denied'); ?>
<div class="grid-wrap">
<div class="catalog-index">
<?php if(count($categories_name)>1): ?>
<div class="croshca">
    <a href="<?=PATH?>">Гглавная</a> / <a href="?view=catalog&amp;category=<?=$categories_name[0]['categories_id']?>"><?=$categories_name[0]['name']?></a> / <span><?=$categories_name[1]['name']?></span>
</div>
<div class="content-header">
            <h1><?=$categories_name[1]['name']?></h1>
</div> 
<?php elseif(count($categories_name)==1): ?>
<div class="croshca">
    <a href="<?=PATH?>">Гглавная</a> / <span><?=$categories_name[0]['name']?></span>
</div>
<div class="content-header">
            <h1><?=$categories_name[0]['name']?></h1>
</div>
<?php endif; ?>

    <div class="vid-sort">
            Вид: 
                <a href="#" id="grid" class="grid_list"><img src="<?=TEMPLATE?>images/view-tabel-act.png" alt="табличный вид" /></a> 
                <a href="#" id="list" class="grid_list"><img src="<?=TEMPLATE?>images/view-line-act.png" alt="линейный вид" /></a>  
                &nbsp;&nbsp;           
                Сортировать по:&nbsp;   
                <a id="param_order" class="sort-top"><?=$order?></a>
                <div class="sort-wrap">
                    <?php foreach($order_p as $key => $value): ?>
                    <?php if($value[0] == $order) continue; ?>
                    <a href="?view=catalog&amp;category=<?=$category?>&amp;order=<?=$key?>&amp;page=<?=$page?>" class="sort-bot"><?=$value[0]?></a>                        
                    <?php endforeach; ?>
                </div> 
            </div>    
    <?php if($products): // если получены товары категории ?>
    <?php foreach($products as $product): ?>
        <?php if(!isset($_COOKIE['display']) OR $_COOKIE['display'] == 'grid'): // если вид - сетка ?>
        <!-- Табличный вид продуктов -->	 
			<div class="product-index bp1-col-one-half bp2-col-one-third bp3-col-one-third">
                    <div class="info-goods">
                        <a href="?view=product&amp;goods_id=<?=$product['goods_id']?>"><img src="<?=PRODUCTIMG?><?=$product['image']?>" width="90" alt="" /></a>
                        <h2><a href="?view=product&amp;goods_id=<?=$product['goods_id']?>"><?=$product['name']?></a></h2>
                    </div>
                    <div class="block_addtocart">
                        <p>Цена :  <span><?=$product['price']?></span> руб.</p>
                        <div class="btn_content"><a href="?view=addtocart&amp;goods_id=<?=$product['goods_id']?>">В корзину</a></div>
                    </div>  
			</div>
            
            <!-- Табличный вид продуктов -->
                    <?php else: // если линейный вид ?>
            <!-- Линейный вид продуктов -->
            <div class="product-line">
                <div class="product-line-har">
                    <a href="?view=product&amp;goods_id=<?=$product['goods_id']?>"><img src="<?=PRODUCTIMG?><?=$product['image']?>" width="48" alt="" /></a>
                    <p>Цена :  <span><?=$product['price']?></span></p>
                    <div class="btn_content"><a href="?view=addtocart&amp;goods_id=<?=$product['goods_id']?>">В корзину</a></div>
                    <p class="cat-line-more"><a href="?view=product&amp;goods_id=<?=$product['goods_id']?>">подробнее...</a></p>
                </div>	
                <div class="product-line-opis">
                    <h2 class="product-name"><a href="?view=product&amp;goods_id=<?=$product['goods_id']?>"><?=$product['name']?></a></h2>
                    <p><?=$product['anons']?></p>
                </div>
            </div>
        <!-- Линейный вид продуктов -->
        <?php endif; // конец условия переключателя видов  ?>
    <?php endforeach; ?>
    <div class="clr"></div>
    <?php if($pages_count > 1) pagination($page, $pages_count); ?>
    <?php else: ?>
        <p class="error">Товаров нет!</p>
    <?php endif; ?>	
		</div>
</div>
    			