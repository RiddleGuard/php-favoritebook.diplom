<?php session_start();
// подключение модели
require_once MODEL;

// подключение библиотеки функций
require_once 'functions/functions.php';

// получение массива каталога
$cat = catalog();
// получение массива страниц
$pages = pages();
//последние 5 новостей
$news = news_five();
// регистрация
if($_POST['reg']){
    registration();
}
// авторизация
if($_POST['auto']){
    authorization();
    if($_SESSION['auth']['user']){
        // если пользователь авторизовался
        redirect('/');
        }
}

// выход пользователя
if($_GET['do'] == 'logout'){
    logout();
    redirect();
} 
//Сортировка
$order_p = array(
                'pricea' => array('Дешевые','price ASC'),
                'priced' => array('Дорогие','price DESC'),
                'Datea' => array('по дате к последним','date ASC'),
                'Dated' => array('по дате с последних','date DESC'),
                'namea' => array('от А до Я','name ASC'),
                'named' => array('от Я до А','name DESC'),
                );

//----------------------------------------------------------------------------
        

//----------------------------------------------------------------------------

// получение динамичной части шаблона #content
$view = empty($_GET['view']) ? 'tamplate' : $_GET['view'];

 switch($view){
    case('tamplate'):
        //главная
        //сортировка
        $order_get = clear($_GET['order']);
        if(array_key_exists($order_get,$order_p)){
            //По умолчанию
            $order = $order_p[$order_get][0];
            $order_db = $order_p[$order_get][1];
        }else{
            //сортировка
            $order = $order_p['namea'][0];
            $order_db = $order_p['namea'][1];
        }
        // параметры для навигации
        $perpage = PERPAGE; // кол-во новостей на страницу
        if(isset($_GET['page'])){
            $page = (int)$_GET['page'];
            if($page < 1) $page = 1;
        }else{
            $page = 1;
        }
        $count_rows = count_goods(); // общее кол-во новостей
        $pages_count = ceil($count_rows / $perpage); // общее кол-во страниц
        if(!$pages_count) $pages_count=1; //минимум одна страница
        if($page > $pages_count) $page = $pages_count;
        $start_pos = ($page -1)*$perpage; //начальная позиция для запроса
        
        $all_goods = ALL_goods($order_db,$start_pos,$perpage);
    break;
    case('catalog'):
        // товары категории
        $category = abs((int)$_GET['category']);
        //сортировка
        $order_get = clear($_GET['order']);
        if(array_key_exists($order_get,$order_p)){
            //По умолчанию
            $order = $order_p[$order_get][0];
            $order_db = $order_p[$order_get][1];
        }else{
            //сортировка
            $order = $order_p['namea'][0];
            $order_db = $order_p['namea'][1];
        }
        // параметры для навигации
        $perpage = PERPAGE;// кол-товаров на страницу
        if(isset($_GET['page'])){
            $page = (int)$_GET['page'];
            if($page < 1) $page =1;
        }else{
            $page =1;
        }
        $count_rows = count_rows($category); //кол-во товаров на всех страницах
        $pages_count = ceil($count_rows / $perpage); // общее кол-во страниц
        if(!$pages_count) $pages_count=1; //минимум одна страница
        if($page > $pages_count) $page = $pages_count;
        $start_pos = ($page -1)*$perpage; //начальная позиция для запроса
        
        $categories_name=categories_name($category);//имя категоии
        $products = products($category,$order_db, $start_pos, $perpage); // получаем массив из модели
     break;
    case ('search'):
        //Поиск
        //сортировка
        $order_get = clear($_GET['order']);
        if(array_key_exists($order_get,$order_p)){
            //По умолчанию
            $order = $order_p[$order_get][0];
            $order_db = $order_p[$order_get][1];
        }else{
            //сортировка
            $order = $order_p['namea'][0];
            $order_db = $order_p['namea'][1];
        }
        
        $result_search = search($order_db);
        
        // параметры для навигации
        $perpage = PERPAGE; // кол-во товаров на страницу
        if(isset($_GET['page'])){
            $page = (int)$_GET['page'];
            if($page < 1) $page = 1;
        }else{
            $page = 1;
        }
        $count_rows = count($result_search); // общее кол-во товаров
        $pages_count = ceil($count_rows / $perpage); // кол-во страниц
        if(!$pages_count) $pages_count = 1; // минимум 1 страница
        if($page > $pages_count) $page = $pages_count; // если запрошенная страница больше максимума
        $start_pos = ($page - 1) * $perpage; // начальная позиция для запроса
        $endpos = $start_pos + $perpage; // до какого товара будет вывод на странице
        if($endpos > $count_rows) $endpos = $count_rows;
    break;
    case('addtocart'):
        // добавление в корзину
        $goods_id = abs((int)$_GET['goods_id']);
        addtocart($goods_id);
        
        $_SESSION['total_sum'] = total_sum($_SESSION['cart']);
        
        // кол-во товара в корзине + защита от ввода несуществующего ID товара
        total_quantity();
        redirect();
    break;
    case('product'):
        //Детальный вид
         $goods_id = abs((int)$_GET['goods_id']);
        if($goods_id){
            $goods=get_goods($goods_id);
            if($goods)
            $categories_name=categories_name($goods['categories_id']);//имя категоии
            
        } 
    break;
    case('news'):
        //отдельная новость
        $news_id = abs((int)$_GET['news_id']);
        $news_text = get_news_text($news_id);
        break;
    case('archive-nwes'):
        //все новости
        $perpage = 6; // кол-во новостей на страницу
        if(isset($_GET['page'])){
            $page = (int)$_GET['page'];
            if($page < 1) $page = 1;
        }else{
            $page = 1;
        }
        $count_rows = count_news(); // общее кол-во новостей
        $pages_count = ceil($count_rows / $perpage); // общее кол-во страниц
        if(!$pages_count) $pages_count=1; //минимум одна страница
        if($page > $pages_count) $page = $pages_count;
        $start_pos = ($page -1)*$perpage; //начальная позиция для запроса
        
        $all_news = get_all_news($start_pos,$perpage);
    break;
    case('cart'):
        // получение способов доставки
        $dostavka = get_dostavka();
        // корзина
        if(isset($_GET['qty'],$_GET['id'])){
            $goods_id = abs((int) $_GET['id']);
            $qty = abs((int)$_GET['qty']);
            
            $qty -=$_SESSION['cart'][$goods_id]['qty'];
            addtocart($goods_id, $qty);
            
            $_SESSION['total_sum'] = total_sum($_SESSION['cart']); // сумма заказа
            
            total_quantity(); // кол-во товара в корзине + защита от ввода несуществующего ID товара
            redirect();
                }
        // удаление товара из корзины
        if(isset($_GET['delete'])){
            $id = abs((int)$_GET['delete']);
            if($id){
                delete_from_cart($id);
            }
            redirect();
        }
        // оформление заказа
        if($_POST['order_x']){
            add_order();
            redirect();
        }
    break;
    case('page'):
        //отдельная старница
        $page_id =abs((int)$_GET['page_id']);
        $get_page = get_page($page_id);
    break;
    case('reg'):
        // регистрация
    break;
    case('autorization'):
        //Авторизация
    break;
    default:
        // если из адресной строки получено имя несуществующего вида
        $view = 'tamplate';
        $ALL_goods = ALL_goods($order_db);
}
// подключени вида
require_once TEMPLATE.'index.php';
?>