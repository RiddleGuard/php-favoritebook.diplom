<?php

defined('ISHOP') or die('Access denied');


/* ===Все товары=== */
function ALL_goods($order_db, $start_pos, $perpage){
    $query = "SELECT goods_id, name, image, price,anons FROM goods
                WHERE visible='1' ORDER BY $order_db LIMIT $start_pos, $perpage";
    $res = mysql_query($query) or die(mysql_error());
    
    $eyestoppers = array();
    while($row = mysql_fetch_assoc($res)){
        $all_goods[] = $row;
    }
    
    return $all_goods;
}
/* ===Все товары=== */

/* ====Каталог - получение массива=== */
function catalog(){
    $query = "SELECT * FROM categories ORDER BY parent_id, categories_id";
    $res = mysql_query($query) or die(mysql_query());
    
    //массив категорий
    $cat = array();
    while($row = mysql_fetch_assoc($res)){
        if(!$row['parent_id']){
            $cat[$row['categories_id']][] = $row['name'];
        }else{
            $cat[$row['parent_id']]['sub'][$row['categories_id']] = $row['name'];
        }
    }
    return $cat;
}
/* ====Каталог - получение массива=== */

/* ==количество товаров в каталоге== */
function count_rows($category){
    $query = "(SELECT COUNT(goods_id) as count_rows
                 FROM goods
                     WHERE categories_id = $category AND visible='1')
               UNION      
               (SELECT COUNT(goods_id) as count_rows
                 FROM goods 
                     WHERE categories_id IN 
                (
                    SELECT categories_id FROM categories WHERE parent_id = $category
                ) AND visible='1')";
    $res = mysql_query($query) or die(mysql_error());
    
    while($row = mysql_fetch_assoc($res)){
        if($row['count_rows']) $count_rows = $row['count_rows'];
    }
    return $count_rows;
}
/* ==количество товаров в каталоге== */

/* ===Получение массива товаров по категории=== */
function products($category,$order_db, $start_pos, $perpage){
    $query = "(SELECT goods_id, name, image, anons, price, categories_id,date
                 FROM goods
                     WHERE categories_id = $category AND visible='1')
               UNION      
               (SELECT goods_id, name, image, anons, price, categories_id,date
                 FROM goods 
                     WHERE categories_id IN 
                (
                    SELECT categories_id FROM categories WHERE parent_id = $category
                ) AND visible='1') ORDER BY $order_db LIMIT $start_pos, $perpage";
    $res = mysql_query($query) or die(mysql_error());
    
    $products = array();
    while($row = mysql_fetch_assoc($res)){
        $products[] = $row;
    }
    
    return $products;
}
/* ===Получение массива товаров по категории=== */

/* ===Получение названий для крошек=== */
function categories_name($category){
    $query="(SELECT categories_id, name FROM categories
                WHERE categories_id = 
                (SELECT parent_id FROM categories WHERE categories_id = $category))
            UNION
            (SELECT categories_id, name FROM categories WHERE categories_id = $category)";
    $res = mysql_query($query) or die(mysql_error());
    $categories_name = array();
    while($row = mysql_fetch_assoc($res)){
        $categories_name[] = $row;
    }
    return $categories_name;
}
/* ===Получение названий для крошек=== */

/* Добавление заказа */
function add_order(){
    $dostavka_id = (int)$_POST['dostavka'];
    if(!$dostavka_id) $dostavka_id = 1;
    $prim = clear($_POST['prim']);
    
    if($_SESSION['auth']['user']) $user_id = $_SESSION['auth']['user_id'];
    if(!$_SESSION['auth']['user']){
        $error = ''; // флаг проверки пустых полей
        
        $name = trim($_POST['name']);
        $email = trim($_POST['email']);
        $phone = trim($_POST['phone']);
        $address = trim($_POST['address']);
        $prim = trim($_POST['prim']);
        
    
        if(empty($name)) $error .= '<li>Не указано ФИО</li>';
        if(empty($email)) $error .= '<li>Не указан Email</li>';
        if(empty($phone)) $error .= '<li>Не указан телефон</li>';
        if(empty($address)) $error .= '<li>Не указан адрес</li>'; 
        
        if(empty($error)){
            // добавляем пользователя в заказчики
            $user_id = add_user($name, $email, $phone, $address,$prim);
            if(!$user_id) return false;        
        }else{
            $_SESSION['order']['res'] = "<div class='error'>Не заполнены обязательные поля: <ul> $error </ul></div>";
            $_SESSION['order']['name'] = $name;
            $_SESSION['order']['email'] = $email;
            $_SESSION['order']['phone'] = $phone;
            $_SESSION['order']['address'] = $address;
            $_SESSION['order']['prim'] = $prim;
            return false;  
        }
    }    
    $_SESSION['order']['email'] = $email;
    save_order($user_id, $dostavka_id,$prim);
    $_SESSION['order']['res'] = "<div class='success'>Заказа успшно проведен. В ближайшее время с вами свяжется менеджер для согласования заказа.</div>";   
}
/* Добавление заказа */

/* ===Сумма заказа в корзине + атрибуты товара===*/
function total_sum($goods){
    $total_sum = 0;
    
    $str_goods = implode(',',array_keys($goods));
    
    $query = "SELECT goods_id, name, price, image
                FROM goods
                    WHERE goods_id IN ($str_goods)";
    $res = mysql_query($query) or die(mysql_error());
    
    while($row = mysql_fetch_assoc($res)){
        $_SESSION['cart'][$row['goods_id']]['name'] = $row['name'];
        $_SESSION['cart'][$row['goods_id']]['price'] = $row['price'];
        $_SESSION['cart'][$row['goods_id']]['image'] = $row['image'];
        $total_sum += $_SESSION['cart'][$row['goods_id']]['qty'] * $row['price'];
    }
    return $total_sum;
}
/* ===Сумма заказа в корзине + атрибуты товара===*/

/* способы доставки */
function get_dostavka(){
    $query = "select * from dostavka";
    $res = mysql_query($query);
    
    $dostavka = array();
    while($row = mysql_fetch_assoc($res)){
        $dostavka[] = $row;
    }
    
    return $dostavka;
}
/* способы доставки */


/* Добавление заказщика-гостя */
function add_user($name, $email, $phone, $address,$prim){
    $name = clear($_POST['name']);
    $email = clear($_POST['email']);
    $phone = clear($_POST['phone']);
    $address = clear($_POST['address']);
    $prim = clear($_POST['prim']);
    $query = "insert into users (name, email, phone, address)
                values ('$name','$email','$phone','$address')";
    $res = mysql_query($query)or die(mysql_error());
    if(mysql_affected_rows() > 0){
        //если гость добавлен в число заказчиков - получаем его id
        return mysql_insert_id();
            
    }else{
        $_SESSION['order']['res'] = "<div class='error'>Сервер вернул неправильные данные: <ul> $error </ul></div>";
        $_SESSION['order']['name'] = $name;
        $_SESSION['order']['email'] = $email;
        $_SESSION['order']['phone'] = $phone;
        $_SESSION['order']['address'] = $address;
        $_SESSION['order']['prim'] = $prim;
        return false;
    }
    
}
/* Добавление заказщика-гостя */

/* Сохранение заказа */
function save_order($user_id, $dostavka_id, $prim){
    $prim = clear($_POST['prim']);
    $query = "INSERT INTO orders (`user_id`, `date`, `dostavka_id`, `prim`) 
                values ($user_id, NOW(), $dostavka_id, '$prim')";
    mysql_query($query) or die(mysql_error());
    if(mysql_affected_rows() == -1){
        //если не получилось сохранить заказ - удаляем заказчика
        mysql_query("DELETE FROM users 
        WHERE user_id = $user_id and login = ''");
        $_SESSION['order']['res'] = "<div class='error'>Ошибка добавления заказа.Возможно не выбран сособ доставки</div>";
        return false;
    }
    $order_id = mysql_insert_id();// ИД последнего заказа
    foreach($_SESSION['cart'] as $goods_id => $value){
        $val .="($order_id,$goods_id, {$value['qty']},'{$value['name']}',{$value['price']}),";
    
    }
    $val = substr($val,0 , -1);//удаляем запятую в конце
    
    $query = "INSERT INTO order_goods (order_id, goods_id, quantity,name,price)
               values $val";
    mysql_query($query) or die(mysql_error());
    if(mysql_affected_rows() == -1){
        //если не выгрузился заказ - удаляем заказчика и заказ
        mysql_query("DELETE FROM order 
        where order_id = $order_id");
        mysql_query("DELETE FROM users 
        WHERE user_id = $user_id and login = ''");
        $_SESSION['order']['res'] = "<div class='error'>Ошибка добавления списка заказов.</div>";
        return false;
    }
    if($_SESSION['auth']['email']) $email = $_SESSION['auth']['email'];
        else $email = $_SESSION['order']['email'];
    mail_order($order_id, $email);
    
    //если заказ выгрузился 
    unset($_SESSION['cart']);
    unset($_SESSION['total_sum']);
    unset($_SESSION['total_quantity']);
    return true;
    
}
/* Сохранение заказа */

/* Отправка уведомлений о заказе на Email */
function mail_order($order_id, $email){
    //mail(to, subject, body,header);
    //тема письма
    $subject = "Заказ в интернет-магазине";
    //загаловки
    $headers .= "Content-type: text/plain; charset=utf-8\r\n";
    $headers .= "From Favorite BOOK";
    //Тема письма
    $mail_body = "Благодарим вас за заказ!\r\nНомер вашего заказа - №{$order_id}\r\n\r\n
                  Заказанные товары:\r\n";
                  foreach($_SESSION['cart'] as $goods_id => $value){
                  $mail_body .= "Наименование: {$value['name']}, Цена: {$value['price']}, Количество: {$value['qty']} шт.\r\n";
    
                  }
                  $mail_body .="\r\nИтого: {$_SESSION['total_quantity']}\r\nНа сумму: {$_SESSION['total_sum']}\r\n";
    // отправка писем
    mail($email, $subject, $mail_body, $headers);
    mail(ADMIN_EMAIL, $subject, $mail_body, $headers);

}
/* Отправка уведомлений о заказе на Email */

/* ===Регистрация=== */
function registration(){
    $error = ''; // флаг проверки пустых полей
    
    $login = trim($_POST['login']);
    $pass = trim($_POST['pass']);
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $phone = trim($_POST['phone']);
    $address = trim($_POST['address']);
    
    if(empty($login)) $error .= '<li>Не указан логин</li>';
    if(empty($pass)) $error .= '<li>Не указан пароль</li>';
    if(empty($name)) $error .= '<li>Не указано ФИО</li>';
    if(empty($email)) $error .= '<li>Не указан Email</li>';
    if(empty($phone)) $error .= '<li>Не указан телефон</li>';
    if(empty($address)) $error .= '<li>Не указан адрес</li>';
    
    if(empty($error)){
        // если все поля заполнены
        // проверяем нет ли такого юзера в БД
        $query = "SELECT user_id FROM users WHERE login = '$login' LIMIT 1";
        $res = mysql_query($query) or die(mysql_error());
        $row = mysql_num_rows($res); // 1 - такой юзер есть, 0 - нет
        if($row){
            // если такой логин уже есть
            $_SESSION['reg']['res'] = "<div class='error'>Пользователь с таким логином уже зарегистрирован на сайте. Введите другой логин.</div>";
            $_SESSION['reg']['name'] = $name;
            $_SESSION['reg']['email'] = $email;
            $_SESSION['reg']['phone'] = $phone;
            $_SESSION['reg']['address'] = $address;
        }else{
            // если все ок - регистрируем
            $login = clear($_POST['login']);
            $name = clear($_POST['name']);
            $email = clear($_POST['email']);
            $phone = clear($_POST['phone']);
            $address = clear($_POST['address']);
            $pass = md5($pass);
            $query = "INSERT INTO users (name, email, phone, address, login, password)
                        VALUES ('$name', '$email', '$phone', '$address', '$login', '$pass')";
            $res = mysql_query($query) or die(mysql_error());
            if(mysql_affected_rows() > 0){
                // если запись добавлена
                $_SESSION['reg']['res'] = "<div class='success'>Регистрация прошла успешно.</div>";
                $_SESSION['auth']['login'] = $_POST['login'];
                $_SESSION['auth']['user'] = $name;
                $_SESSION['auth']['user_id']=mysql_insert_id();
                $_SESSION['auth']['email'] = $email;
            }
            else{
                $_SESSION['reg']['res'] = "<div class='error'>Ошибка добавления попробуйте снова.</div>";
                $_SESSION['reg']['login'] = $login;
                $_SESSION['reg']['name'] = $name;
                $_SESSION['reg']['email'] = $email;
                $_SESSION['reg']['phone'] = $phone;
                $_SESSION['reg']['address'] = $address;
            }
        }
    }else{
        // если не заполнены обязательные поля
        $_SESSION['reg']['res'] = "<div class='error'>Не заполнены обязательные поля: <ul> $error </ul></div>";
        $_SESSION['reg']['login'] = $login;
        $_SESSION['reg']['name'] = $name;
        $_SESSION['reg']['email'] = $email;
        $_SESSION['reg']['phone'] = $phone;
        $_SESSION['reg']['address'] = $address;
    }
}
/* ===Регистрация=== */

/* ===Авторизация=== */
function authorization(){
    $login = clear($_POST['login']);
    $pass = trim($_POST['pass']);
    
    if(empty($login) OR empty($pass)){
        // если пусты поля логин/пароль
        $_SESSION['auth']['error'] = "Поля логин/пароль должны быть заполнены!";
    }else{
        // если получены данные из полей логин/пароль
        $pass = md5($pass);  
       
        $query = "SELECT user_id, name, email, id_role FROM users WHERE login = '$login' AND password = '$pass' LIMIT 1";
        $res = mysql_query($query) or die(mysql_error());
        if(mysql_num_rows($res) == 1){
            // если авторизация успешна
            $row = mysql_fetch_row($res);
            $_SESSION['auth']['user_id'] = $row[0];
            $_SESSION['auth']['user'] = $row[1];
            $_SESSION['auth']['email'] = $row[2];
            $_SESSION['auth']['login'] = $_POST['login'];
            if($row[3]==2){
                $_SESSION['auth']['admin'] = $_POST['login'];
            }
        }else{
            // если неверен логин/пароль
            $_SESSION['auth']['error'] = "Логин/пароль введены неверно!";
        }
    }
}
/* ===Авторизация=== */

/* ==Количество товаров==*/
function count_goods(){
    $query = "SELECT COUNT(goods_id) FROM goods WHERE visible='1'";
    $res = mysql_query($query)or die(mysql_error());
    $count_goods = mysql_fetch_row($res);
    return $count_goods[0];
}
/* ==Количество товаров==*/

/* ===Поиск=== */
function search($order_db){
    $search = clear($_GET['search']);
    $result_search = array(); //результат поиска
    
    if(mb_strlen($search, 'UTF-8') < 4){
        $result_search['notfound'] = "<div class='error'>Поисковый запрос должен содержать не менее 4-х символов</div>";
    }else{
        $query = "SELECT goods_id, name, image, price, anons
                    FROM goods
                        WHERE MATCH(name) AGAINST('{$search}*' IN BOOLEAN MODE) AND visible='1' ORDER BY $order_db";
        $res = mysql_query($query) or die(mysql_error());
        
        if(mysql_num_rows($res) > 0){
            while($row_search = mysql_fetch_assoc($res)){
                $result_search[] = $row_search;
            }
        }else{
            $result_search['notfound'] = "<div class='error'>По Вашему запросу ничего не найдено</div>";
        }
    }
    
    return $result_search;
}
/* ===Поиск=== */

/* ==Страницы имена==*/
function pages(){
    $query= "Select page_id,title from pages ORDER BY position";
    $res = mysql_query($query)or die(mysql_error());
    $pages = array();
    while($row=mysql_fetch_assoc($res)){
        $pages[]=$row;
    }
    return $pages;
}
/* ==Страницы имена==*/
/*==новости=*/
function get_news_text($news_id){
    $query = "SELECT title, text, date FROM news WHERE news_id =$news_id";
      $res = mysql_query($query)or die(mysql_error());
    $news_text = array();
    $news_text=mysql_fetch_assoc($res);
    
    return $news_text;
}
/*==новости=*/

/* ===Последние пять новостей=== */
function news_five(){
    $query = "SELECT news_id,title, date FROM news
                ORDER BY date DESC,news_id DESC LIMIT 5";
    $res = mysql_query($query) or die(mysql_error());
    
    $news_five = array();
    while($row = mysql_fetch_assoc($res)){
        $news_five[] = $row;
    }
    
    return $news_five;
}
/* ===Последние пять новостей=== */

/*==все новости=*/
function get_all_news($start_pos,$perpage){
     $query = "SELECT  date,news_id,title, anons FROM news ORDER BY date DESC,news_id DESC LIMIT $start_pos, $perpage";
      $res = mysql_query($query)or die(mysql_error());
    $all_new = array();
    while($row=mysql_fetch_assoc($res)){
        $all_new[]=$row;
    }
    return $all_new;
}
/*==все новости=*/
/*==кол-во новостей==*/
function count_news(){
    $query = "SELECT COUNT(news_id) FROM news";
      $res = mysql_query($query)or die(mysql_error());
      $count_news = mysql_fetch_row($res);
      return $count_news[0];
}
/*==кол-во новостей==*/
/* ==Страницы==*/
function get_page($page_id){
    $query = "SELECT title,text FROM pages 
                WHERE page_id=$page_id";
    $res = mysql_query($query)or die (mysql_error());
    $get_page = array();
    $get_page = mysql_fetch_assoc($res);
    return $get_page; 
}
/* ==Страницы==*/

/* ===Детальный вид=== */
function get_goods($goods_id){
    $query = "SELECT * FROM goods WHERE goods_id = $goods_id AND visible = '1'";
    $res = mysql_query($query)or die (mysql_error());
    
    $goods = array();
    $goods = mysql_fetch_assoc($res);
    
    return $goods;
}
/* ===Детальный вид=== */
